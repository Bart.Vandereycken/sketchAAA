function verify_small_samples()
% Verification of the small sample bounds in the paper (failure probality).

% BV - 12.2023

% buckling plate
[F, n, pname, Z, ~, ~] = get_nlevp_problem(2);

% evaluate vectorized F on Z
FZ = zeros(n*n, length(Z));
for i=1:length(Z)
    FZi = F(Z(i)); FZ(:,i) = FZi(:);
end

% compute barycentric form of AAA approximation R for F
max_deg = 25; opts.tol = 0;
[z,w,ind,stats_faster] = sketchAAA(FZ,Z,max_deg, opts);
R = @(zz) eval_bary_vec(zz, z, w, FZ(:,ind));
deg = length(z)

% compute max error wrt Z
E = R(Z) - FZ;
e = vecnorm(E,'inf');
max_err = max(e);
l2_err = norm(E,'fro');

% stable rank estimate
rho = norm(E,'fro')^2 / norm(E, 2)^2

ells = [1 2 4 8 16]
Emp_under = [];
The_under = [];
Emp_over = [];
The_over = [];
Emp_both = [];
The_both = [];
for ell=ells
    ell
    [emp_under, the_under, emp_over, the_over, emp_both, the_both] = empiricial_distrib_estimator(FZ, z, w, ind, Z, ell, l2_err, rho);
    Emp_under = [Emp_under; emp_under];
    The_under = [The_under; the_under];
    Emp_over = [Emp_over; emp_over];
    The_over = [The_over; the_over];
    Emp_both = [Emp_both; emp_both];
    The_both = [The_both; the_both];
end
% each row is for a fixed ell
Emp_under
The_under
Emp_over
The_over
save('results/verify_small_samples.mat')

Ell = repmat(ells', [1 size(Emp_under,2)]);
D = [Ell(:) Emp_under(:) The_under(:) Emp_over(:) The_over(:) Emp_both(:) The_both(:)]';
tab = sprintf('& %d & %0.3f & %0.3f & %0.3f & %0.3f & %0.3f & %0.3f || \n',D(:));
fprintf(strrep(tab,"|", "\\"))

end

function [emp_under, the_under, emp_over, the_over, emp_both, the_both] = empiricial_distrib_estimator(FZ, z, w, ind, Z, ell, l2_err, rho)
% construct surrogates 
c = 1; % real
NB_REPS = 1e5;
N = size(FZ,1);
for rep = 1:NB_REPS
    rng(rep)
    UV = randn(N,ell); UV = UV/sqrt(ell); % UV = orth(UV);
    GZ = UV'*FZ;           
    Rg = @(zz) eval_bary_vec(zz, z, w, GZ(:,ind));
    
    Eg = Rg(Z) - GZ;
    eg = vecnorm(Eg,'inf');
    max_err_g(rep) = max(eg);
    l2_err_g(rep) = norm(Eg,'fro');
end

% emp_prob
taus = [2 3 5 10];
for i=1:length(taus)
    tau = taus(i);

    % underestimation correct within faction 1/tau
    emp_under(i) = sum(l2_err/tau <=  l2_err_g) / length(l2_err_g); 

    % prob of understimating (3.2) (MATLAB directly uses regularized one and switches arguments)
    prob_under = gammainc(c*ell*rho*tau^(-2)/2, c*ell/2);
    the_under(i) = 1 - prob_under;
    
    % overestimation correct within faction tau
    emp_over(i) = sum(l2_err_g <= tau * l2_err) / length(l2_err_g);
    % 1 - (3.3): this is prob(estimator <= tau * exact) - hence prob of
    prob_over = exp(-c*ell/2*rho*(tau-1)^2);    
    the_over(i) = 1 - prob_over;

    % both sides correct
    mask = (l2_err_g <= tau * l2_err) .* (l2_err/tau <=  l2_err_g);
    emp_both(i) = sum(mask) / length(l2_err_g);
    the_both(i) = 1 - prob_over - prob_under;

end
end 