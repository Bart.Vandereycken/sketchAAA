%EVALCHEBPOLY  Evaluate polynomial given in Chebyshev basis.
%
%   V = EVALCHEBPOLY(P, X, L) evaluates the polynomial specified by P
%   at the pair (X, L).  Considering an expansion of the polynomial in
%   the Chebyshev basis, P(:, :, k) contains the coefficient matrix
%   associated with the k-th Chebyshev polynomial of the first kind.
%   The computed result satisfies
%
%      V = P(:, :, 1)*X*T_1(L) + ... + P(:, :, d)*X*T_d(L),
%
%   where d = size(P, 3) and T_k is the k-th Chebyshev polynomial of
%   the first kind.  T_k(L) is to be understood as a matrix function
%   in the usual sense.

% Cedric Effenberger
% ETH Zurich, October 2011.
function V = evalchebpoly(P, X, L)

% determine polynomial degree
d1 = size(P, 3);

% evaluate polynomial
T0 = X;
T1 = X*L;
V = P(:, :, 1) * T0 + P(:, :, 2) * T1;

for k = 3:d1
	
	T2 = 2*T1*L - T0;
	T0 = T1;
	T1 = T2;
	V = V + P(:, :, k) * T2;
end