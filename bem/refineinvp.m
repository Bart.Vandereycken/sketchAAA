%REFINEINVP  Newton-based refinement of invariant pairs for polynomial
%eigenvalue problems.
%
%   [X, L] = REFINEINVP(P, X, L) performs one step of Newton-based
%   iterative refinement for an approximate invariant pair (X, L)
%   of the polynomial eigenvalue problem specified by P.  Considering
%   an expansion of P in the Chbeyshev basis, P(:, :, k) contains the
%   coefficient matrix associated with the k-th Chebyshev polynomial
%   of the first kind.

% Cedric Effenberger
% ETH Zurich, October 2011.
function [X, L] = refineinvp(P, X, L)

% transform to Schur form
[Q, L] = schur(L, 'complex');
X = X*Q;

% compute residual
R = evalchebpoly(P, X, L);

% compute Newton correction
[DX, DL] = newtcorr(P, X, L, -R, zeros(size(L)));

% update
X = X + DX;
L = L + DL;