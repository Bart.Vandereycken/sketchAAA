% TRIPANEL  Class representing a triangular boundary element.

% Cedric Effenberger and Michael Steinlechner
% ETH Zurich, October 2011.
classdef TriPanel
    properties
        index
        % the vertices as ROW vectors
        P1
        P2
        P3
        % global node numbers
        nodes
        % midpoint
        midpoint
        % area of the panel
        area
        % normal vector to the triangle
        normal
        % tangent vectors to edges {1,2,3} of the triangle
        tau1
        tau2
        tau3
        % normal vectors to edges {1,2,3} of the triangle
        nu1
        nu2
        nu3
        % gauss points and weights
        gaussP
        gaussW
    end
    methods
        function obj = TriPanel(n)
            if nargin ~= 0
                obj(n,1) = TriPanel;
                for i=1:n
                    obj(i,1).index = i;
                end
            end
        end
        
        function x = getX(obj)
            x = [obj.P1(1);obj.P2(1);obj.P3(1)];
        end
        function y = getY(obj)
            y = [obj.P1(2);obj.P2(2);obj.P3(2)];
        end
        function z = getZ(obj)
            z = [obj.P1(3);obj.P2(3);obj.P3(3)];
        end
    end    
end