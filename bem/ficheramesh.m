%FICHERAMESH  Create a boundary mesh for the Fichera corner.
%
%   MESH = FICHERAMESH(N)  constructs a boundary mesh for the Fichera corner
%   [0,1]^3 \ [.5,1]^3.  The boundary of the Fichera corner is divided
%   uniformly into 6*N^2 squares, which are then split into 4 triangles each,
%   resulting in a total number of 24*N^2 triangles.

% Cedric Effenberger and Michael Steinlechner
% ETH Zurich, October 2011.
function mesh = ficheramesh(N)

% check whether N is odd
if (mod(N, 2) ~= 0)
	
	% increment to make N even
	N = N + 1;
end

% store N/2 for later reference
n = N / 2;

% calculate total number of triangles
nTri = 24*N*N;

% construct array of triangluar elements (of class TriPanel)
mesh = TriPanel(nTri);

% compute the area of each triangle
area = 0.25/N/N;

% set up a uniform grid on [0, 1]
a = (0:N) / N;

% initialize counter
id = 0;

% left face
for ii = 1:N
	
	for jj = 1:N
		
		center = [0; mean(a(ii:ii+1)); mean(a(jj:jj+1))];
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii+1); a(jj)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii+1); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii+1); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii+1); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii); a(jj)];
		mesh(id).area = area;
	end
end

%  front face
for ii = 1:N
	
	for jj = 1:N
		
		center = [mean(a(jj:jj+1)); 0; mean(a(ii:ii+1))];
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 0; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 0; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 0; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 0; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 0; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 0; a(ii)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 0; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 0; a(ii)];
		mesh(id).area = area;
	end
end

% bottom face
for ii = 1:N
	
	for jj = 1:N
		
		center = [mean(a(ii:ii+1)); mean(a(jj:jj+1)); 0];
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj); 0];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj+1); 0];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj+1); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj+1); 0];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj+1); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj); 0];
		mesh(id).area = area;
	end
end

% right face

% (0, 1/2) x (0, 1/2)
for ii = 1:n
	
	for jj = 1:n
		
		center = [1; mean(a(ii:ii+1)); mean(a(jj:jj+1))];
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj)];
		mesh(id).area = area;
	end
end

% (1/2, 1) x (0, 1/2)
for ii = n+1:N
	
	for jj = 1:n
		
		center = [1; mean(a(ii:ii+1)); mean(a(jj:jj+1))];
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj)];
		mesh(id).area = area;
	end
end

% (0, 1/2) x (1/2, 1)
for ii = 1:n
	
	for jj = n+1:N
		
		center = [1; mean(a(ii:ii+1)); mean(a(jj:jj+1))];
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj)];
		mesh(id).area = area;
	end
end

% (1/2, 1) x (1/2, 1)
for ii = n+1:N
	
	for jj = n+1:N
		
		center = [0.5; mean(a(ii:ii+1)); mean(a(jj:jj+1))];
		
		id = id + 1;
		mesh(id).P1 = [0.5; a(ii); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0.5; a(ii); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0.5; a(ii); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0.5; a(ii+1); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0.5; a(ii+1); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0.5; a(ii+1); a(jj)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0.5; a(ii+1); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0.5; a(ii); a(jj)];
		mesh(id).area = area;
	end
end

% back face

% (0, 1/2) x (0, 1/2)
for ii = 1:n
	
	for jj = 1:n
		
		center = [mean(a(jj:jj+1)); 1; mean(a(ii:ii+1))];
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii)];
		mesh(id).area = area;
	end
end

% (1/2, 1) x (0, 1/2)
for ii = n+1:N
	
	for jj = 1:n
		
		center = [mean(a(jj:jj+1)); 1; mean(a(ii:ii+1))];
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii)];
		mesh(id).area = area;
	end
end

% (0, 1/2) x (1/2, 1)
for ii = 1:n
	
	for jj = n+1:N
		
		center = [mean(a(jj:jj+1)); 1; mean(a(ii:ii+1))];
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii)];
		mesh(id).area = area;
	end
end

% (1/2, 1) x (1/2, 1)
for ii = n+1:N
	
	for jj = n+1:N
		
		center = [mean(a(jj:jj+1)); 0.5; mean(a(ii:ii+1))];
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 0.5; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 0.5; a(ii)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 0.5; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 0.5; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 0.5; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 0.5; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 0.5; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 0.5; a(ii)];
		mesh(id).area = area;
	end
end

% top face

% (0, 1/2) x (0, 1/2)
for ii = 1:n
	
	for jj = 1:n
		
		center = [mean(a(ii:ii+1)); mean(a(jj:jj+1)); 1];
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj); 1];
		mesh(id).area = area;
	end
end

% (1/2, 1) x (0, 1/2)
for ii = n+1:N
	
	for jj = 1:n
		
		center = [mean(a(ii:ii+1)); mean(a(jj:jj+1)); 1];
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj); 1];
		mesh(id).area = area;
	end
end

% (0, 1/2) x (1/2, 1)
for ii = 1:n
	
	for jj = n+1:N
		
		center = [mean(a(ii:ii+1)); mean(a(jj:jj+1)); 1];
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj); 1];
		mesh(id).area = area;
	end
end

% (1/2, 1) x (1/2, 1)
for ii = n+1:N
	
	for jj = n+1:N
		
		center = [mean(a(ii:ii+1)); mean(a(jj:jj+1)); 0.5];
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj); 0.5];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj+1); 0.5];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj+1); 0.5];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj+1); 0.5];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj+1); 0.5];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj); 0.5];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj); 0.5];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj); 0.5];
		mesh(id).area = area;
	end
end

% compute midpoints, tangents, and normals
for id = 1:nTri
	
	% determine midpoint of triangle
	mesh(id).midpoint = (mesh(id).P1 + mesh(id).P2 + mesh(id).P3) / 3;
	
	% compute normalized tangent vectors
	mesh(id).tau1 = mesh(id).P2 - mesh(id).P1;
	mesh(id).tau2 = mesh(id).P3 - mesh(id).P2;
	mesh(id).tau3 = mesh(id).P1 - mesh(id).P3;
	
	mesh(id).tau1 = mesh(id).tau1 / norm(mesh(id).tau1);
	mesh(id).tau2 = mesh(id).tau2 / norm(mesh(id).tau2);
	mesh(id).tau3 = mesh(id).tau3 / norm(mesh(id).tau3);

	% compute exterior unit normal of triangle
	mesh(id).normal = cross( mesh(id).tau1, mesh(id).tau2 );
	mesh(id).normal = mesh(id).normal / norm(mesh(id).normal );

	% compute exterior unit normals of the edges
	mesh(id).nu1 = cross( mesh(id).tau1, mesh(id).normal );
	mesh(id).nu2 = cross( mesh(id).tau2, mesh(id).normal );
	mesh(id).nu3 = cross( mesh(id).tau3, mesh(id).normal );
	
	mesh(id).nu1 = mesh(id).nu1 / norm( mesh(id).nu1 );
	mesh(id).nu2 = mesh(id).nu2 / norm( mesh(id).nu2 );
	mesh(id).nu3 = mesh(id).nu3 / norm( mesh(id).nu3 );
end