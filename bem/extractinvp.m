%EXTRACTINVP  Extract invariant pair for polynomial eigenvalue problem from
%invariant pair of linearization
%
%   [X, L] = EXTRACTINVP(Y, L, P, SCHEME) extracts an invariant pair (X, L)
%   for the polynomial eigenvalue problem specified by P from the invariant
%   pair (Y, L) of its linearization.  Considering an expansion of the
%   polynomial in the Chebyshev basis, P(:, :, k) contains the coefficient
%   matrix associated with the k-th Chebyshev polynomial of the first kind.
%   Different extraction schemes can be chosen by the parameter SCHEME.  The
%   options are:
%
%      'FIRSTBLOCK' - extraction from uppermost block of Y
%      'GSVD'       - generalized-SVD-based extraction scheme from [1]
%      'STRUCTURED' - structured extraction scheme from [1]
%
% [1] Betcke, T.; Kressner, D. (2011).
% "Perturbation, extraction and refinement of invariant pairs
% for matrix polynomials".
% Linear Algebra and its Applications 435(3): 514-536.

% Cedric Effenberger
% ETH Zurich, October 2011.
function [X, L] = extractinvp(Y, L, P, scheme)

% if SCHEME is not supplied, set to default
if (nargin < 4)
	
	scheme = 'firstblock';
end

% determine problem size
[n, m] = size(Y);
d = size(P, 3) - 1;
n = n / d;

% distinguish between different extraction schemes
switch upper(scheme)
	
	case 'FIRSTBLOCK'
		% extract invariant pair from first block
		X = Y(1:n, :);
		
	case 'GSVD'
		% perform extraction by GSVD
		T0 = Y;
		T1 = Y*L;
		M = P(:, :, 1) * reshape(permute(reshape(T0, n, d, m), [1 3 2]), n, d*m) + ...
			P(:, :, 2) * reshape(permute(reshape(T1, n, d, m), [1 3 2]), n, d*m);
		
		for k = 3:d+1
			
			T2 = 2*T1*L - T0;
			T0 = T1;
			T1 = T2;
			M = M + P(:, :, k) * reshape(permute(reshape(T2, n, d, m), [1 3 2]), n, d*m);
		end
		
		M = reshape(M, n*m, d);
		N = reshape(permute(reshape(Y, n, d, m), [1 3 2]), n*m, d);
		
		[ignore, M] = qr(M, 0);
		[ignore, R] = qr(N, 0);
		[ignore, ignore, Q, ignore, ignore] = gsvd(R, M);
		[Q, ignore] = qr(Q);
		X = reshape(N * Q(:, end), n, m);
		
	case 'STRUCTURED'
		% perform structured extraction
		LL = eye(size(L));
		YL = Y((P-1)*n+1:end, :);
		
		for k = P-1:-1:1
			
			LL = L * LL * L' + eye(size(L));
			YL = YL * L' + Y((k-1)*n+1:k*n, :);
		end
		
		X = YL / LL;
end