%CHEBINTERP  Compute interpolating polynomial for T(lambda).
%
%   P = CHEBINTERP(INTERVAL, DEG, MESH, GAUSS) approximates the nonlinear
%   function T(lambda) representing the BEM discretization of the boundary
%   integral formulation of the Laplace eigenvalue problem.
%   The approximation is performed with an interpolating polynomial of
%   degree DEG, matching T at the Chebyshev nodes in the given INTERVAL.
%   The output P contains the coefficient matrices of the polynomial with
%   respect to the polynomial basis formed by the first DEG+1 Chebyshev
%   polynomials of the first kind.  P(:, :, k) is associated with the
%   k-th Chebyshev polynomial. Note that the original interval is
%   transformed to the interval [-1,1].

% Cedric Effenberger
% ETH Zurich, October 2011.
function P = chebinterp(interval, deg, mesh, gauss)

% compute chebyshev nodes on given interval
chebnodes = (interval(2) - interval(1))/2 * cos(pi/deg * (0:deg).') + (interval(1) + interval(2))/2;

% evaluate T at Chebyshev nodes
T = assembleT(chebnodes, mesh, gauss);

% determine number of triangles in the mesh
nTri = length(mesh);

% set up DCT matrix
C = (2/deg) * cos((pi/deg * (0:deg).') * (0:deg));
C([1, end], :) = C([1, end], :) / 2;
C(:, [1, end]) = C(:, [1, end]) / 2;

% multiply by DCT matrix to obtain polynomial coefficients
P = permute(reshape(C * reshape(permute(T, [3 1 2]), deg+1, []), deg+1, nTri, nTri), [2 3 1]);