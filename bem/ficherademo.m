%FICHERADEMO  Solve the Laplace eigenvalue problem for the Fichera corner
%by means of a boundary element formulation.
%
%   FICHERADEMO performs the following steps:
%
%      - construction of a boundary mesh for the Fichera corner
%      - Chebyshev interpolation of the nonlinear matrix-valued function
%        resulting from the boundary formulation of the problem
%      - solution of the corresponding polynomial eigenvalue problem
%      - iterative refinement of the solution

% Cedric Effenberger
% ETH Zurich, October 2011.

% generate boundary mesh for Fichera corner
fprintf('Generating boundary mesh ...              '); tic
mesh = genmesh(4, 'fichera');
fprintf('Done in %.6f seconds.\n\n', toc);

% compute Chebyshev approximation to boundary element discretization
fprintf('Constructing Chebyshev interpolant ...    '); tic
P = chebinterp([6, 10], 12, mesh, 3);
fprintf('Done in %.6f seconds.\n\n', toc);

% solve polynomial eigenvalue problem in Chebyshev basis by applying
% an Arnoldi method (Krylov-Schur algorithm) to a linearization
fprintf('Solving polynomial eigenvalue problem ... '); tic
[Y, L] = polysolve(P);
fprintf('Done in %.6f seconds.\n\n', toc);

% extract invariant pair for polynomial from (Y, L)
[X, L] = extractinvp(Y, L, P, 'firstblock');

% compute residual of obtained invariant pair
fprintf('Computing residual ...                    '); tic
R = evalchebpoly(P, X, L);
fprintf('Done in %.6f seconds.\n\n', toc);

% two steps of Newton-based iterative refinement
fprintf(['Performing iterative refinement\n\n', ...
	'#refinements    residual norm    comp. time (s)\n' ...
	'-----------------------------------------------\n', ...
	'           0    %13.6e\n'], norm(R, 'fro'));
for it = 1:2
	
	% refine invariant pair
	tic
	[X, L] = refineinvp(P, X, L);
	time = toc;
	
	% compute new residual
	R = evalchebpoly(P, X, L);
	fprintf('%12u    %13.6e    %14.6f\n', it, norm(R, 'fro'), time);
end

% extract eigenvalues from computed invariant pair
fprintf('\nThe following eigenvalues have been computed:\n');
lambda = extractev(L, [6, 10]);
fprintf('%15.6f %+.6ei\n', [real(lambda).'; imag(lambda).']);