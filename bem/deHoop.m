%DEHOOP  Compute integrals of the singular kernel 1 / norm(X - Y).
%
%   F = DEHOOP(X, TRI) computes the integral of 1 / norm(X - Y) for the
%   given X over the triangle TRI by means of the de Hoop formula [1].
%
% [1] A.T. de Hoop (1982).
% "The vector integral-equation method for computing three-dimensional
% magnetic fields".
% Integral Equations and Operator Theory, 5(1): 458-474.

% Cedric Effenberger and Michael Steinlechner
% ETH Zurich, October 2011.
function F = deHoop(x, tri)

npt = size(x, 2);

R1 = tri.P1(:, ones(1, npt)) - x;
R2 = tri.P2(:, ones(1, npt)) - x;
R3 = tri.P3(:, ones(1, npt)) - x;

normR1 = sqrt(sum(R1.^2, 1));
normR2 = sqrt(sum(R2.^2, 1));
normR3 = sqrt(sum(R3.^2, 1));

dist = abs(tri.normal' * R1);

solang = solidAngle(R1, R2, R3);

dot_R1_Nu1 = tri.nu1' * R1;
dot_R2_Nu2 = tri.nu2' * R2;
dot_R3_Nu3 = tri.nu3' * R3;

dot_R1_Tau1 = tri.tau1' * R1;
dot_R2_Tau2 = tri.tau2' * R2;
dot_R3_Tau3 = tri.tau3' * R3;

dot_R2_Tau1 = tri.tau1' * R2;
dot_R3_Tau2 = tri.tau2' * R3;
dot_R1_Tau3 = tri.tau3' * R1;

F = -dist .* solang ...
	+ dot_R1_Nu1 .* log( (normR2 + dot_R2_Tau1) ./ (normR1 + dot_R1_Tau1) ) ...
	+ dot_R2_Nu2 .* log( (normR3 + dot_R3_Tau2) ./ (normR2 + dot_R2_Tau2) ) ...
	+ dot_R3_Nu3 .* log( (normR1 + dot_R1_Tau3) ./ (normR3 + dot_R3_Tau3) );