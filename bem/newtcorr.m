%NEWTCORR  Solve Newton correction equation.
%
%   [DX, DL] = NEWTCORR(P, X, L, R, S) solves the linear system
%
%   DP(X, L)(DX, DL) = R,
%   V(X, L)' * DV(X, L)(DX, DL) = S
%
%   for DX and DL.  The parameter L is assumed to be upper triangular.

% Cedric Effenberger
% ETH Zurich, October 2011.
function [DX, DL] = newtcorr(P, X, L, R, S)

% check if L is upper triangular
if any(any(tril(L, -1)))
	
	error('The input L must be upper triangular!')
end

% determine problem size
[n, m] = size(X);
deg = size(P, 3) - 1;

% allocate memory for results
DX = zeros(size(X));
DL = zeros(size(L));

% perform column-wise forward substitution scheme
for col = 1:m
	
	% compute function value and derivative of Chebyshev polynomials at L
	[tau, Dtau] = chebcol(L, col, deg);
	
	% set up linear system for k-th column of solution
	T0 = X;
	T1 = X*L;
	
	MX = tau(col, 1) * [P(:, :, 1); T0'] + tau(col, 2) * [P(:, :, 2); T1'];
	ML = [P(:, :, 1); T0'] * X * Dtau(:, (col-1)*m+1:col*m, 1) + [P(:, :, 2); T1'] * X * Dtau(:, (col-1)*m+1:col*m, 2);
	R(:, col) = R(:, col) - P(:, :, 1) * (DX(:, 1:col-1) * tau(1:col-1, 1) + X * Dtau(:, 1:(col-1)*m, 1) * reshape(DL(:, 1:col-1), (col-1)*m, 1)) ...
		 - P(:, :, 2) * (DX(:, 1:col-1) * tau(1:col-1, 2) + X * Dtau(:, 1:(col-1)*m, 2) * reshape(DL(:, 1:col-1), (col-1)*m, 1));
	S(:, col) = S(:, col) - T1' * (DX(:, 1:col-1) * tau(1:col-1, 1) + X * Dtau(:, 1:(col-1)*m, 1) * reshape(DL(:, 1:col-1), (col-1)*m, 1)) ...
		 - T1' * (DX(:, 1:col-1) * tau(1:col-1, 2) + X * Dtau(:, 1:(col-1)*m, 2) * reshape(DL(:, 1:col-1), (col-1)*m, 1));
	
	for k = 3:deg+1
		
		T2 = 2*T1*L - T0;
		T0 = T1;
		T1 = T2;
		
		MX = MX + tau(col, k) * [P(:, :, k); T1'];
		ML = ML + [P(:, :, k); T1'] * X * Dtau(:, (col-1)*m+1:col*m, k);
		R(:, col) = R(:, col) - P(:, :, k) * (DX(:, 1:col-1) * tau(1:col-1, k) + X * Dtau(:, 1:(col-1)*m, k) * reshape(DL(:, 1:col-1), (col-1)*m, 1));
		S(:, col) = S(:, col) - T1' * (DX(:, 1:col-1) * tau(1:col-1, k) + X * Dtau(:, 1:(col-1)*m, k) * reshape(DL(:, 1:col-1), (col-1)*m, 1));
	end
	
	% compute k-th column of solution
	D = [MX, ML] \ [R(:, col); S(:, col)];
	DX(:, col) = D(1:n);
	DL(:, col) = D(n+1:end);
end