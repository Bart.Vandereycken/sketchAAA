%UNITCUBEMESH  Create a boundary mesh for the unit cube.
%
%   MESH = UNITCUBEMESH(N)  constructs a boundary mesh for the unit cube [0,1]^3.
%   Every face of the cube is split into NxN squares, which then are divided
%   into 4 triangles each, resulting in 4*N^2 triangles per face and
%   24*N^2 triangles in total.

% Cedric Effenberger and Michael Steinlechner
% ETH Zurich, October 2011.
function mesh = unitcubemesh(N)

% calculate total number of triangles
nTri = 24*N*N;

% construct array of triangluar elements (of class TriPanel)
mesh = TriPanel(nTri);

% compute the area of each triangle
area = 0.25/N/N;

% set up a uniform grid on [0, 1]
a = (0:N) / N;

% initialize counter
id = 0;

% left face
for ii = 1:N
	
	for jj = 1:N
		
		center = [0; mean(a(ii:ii+1)); mean(a(jj:jj+1))];
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii+1); a(jj)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii+1); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii+1); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii+1); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [0; a(ii); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [0; a(ii); a(jj)];
		mesh(id).area = area;
	end
end

%  front face
for ii = 1:N
	
	for jj = 1:N
		
		center = [mean(a(jj:jj+1)); 0; mean(a(ii:ii+1))];
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 0; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 0; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 0; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 0; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 0; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 0; a(ii)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 0; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 0; a(ii)];
		mesh(id).area = area;
	end
end

% bottom face
for ii = 1:N
	
	for jj = 1:N
		
		center = [mean(a(ii:ii+1)); mean(a(jj:jj+1)); 0];
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj); 0];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj+1); 0];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj+1); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj+1); 0];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj+1); 0];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj); 0];
		mesh(id).area = area;
	end
end

% right face
for ii = 1:N
	
	for jj = 1:N
		
		center = [1; mean(a(ii:ii+1)); mean(a(jj:jj+1))];
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii+1); a(jj)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [1; a(ii+1); a(jj)];
		mesh(id).P2 = center;
		mesh(id).P3 = [1; a(ii); a(jj)];
		mesh(id).area = area;
	end
end

% back face
for ii = 1:N
	
	for jj = 1:N
		
		center = [mean(a(jj:jj+1)); 1; mean(a(ii:ii+1))];
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj+1); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj+1); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii+1)];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(jj); 1; a(ii+1)];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(jj); 1; a(ii)];
		mesh(id).area = area;
	end
end

% top face
for ii = 1:N
	
	for jj = 1:N
		
		center = [mean(a(ii:ii+1)); mean(a(jj:jj+1)); 1];
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj+1); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj+1); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii+1); a(jj); 1];
		mesh(id).area = area;
		
		id = id + 1;
		mesh(id).P1 = [a(ii+1); a(jj); 1];
		mesh(id).P2 = center;
		mesh(id).P3 = [a(ii); a(jj); 1];
		mesh(id).area = area;
	end
end

% compute midpoints, tangents, and normals
for id = 1:nTri
	
	% determine midpoint of triangle
	mesh(id).midpoint = (mesh(id).P1 + mesh(id).P2 + mesh(id).P3) / 3;
	
	% compute normalized tangent vectors
	mesh(id).tau1 = mesh(id).P2 - mesh(id).P1;
	mesh(id).tau2 = mesh(id).P3 - mesh(id).P2;
	mesh(id).tau3 = mesh(id).P1 - mesh(id).P3;
	
	mesh(id).tau1 = mesh(id).tau1 / norm(mesh(id).tau1);
	mesh(id).tau2 = mesh(id).tau2 / norm(mesh(id).tau2);
	mesh(id).tau3 = mesh(id).tau3 / norm(mesh(id).tau3);

	% compute exterior unit normal of triangle
	mesh(id).normal = cross( mesh(id).tau1, mesh(id).tau2 );
	mesh(id).normal = mesh(id).normal / norm(mesh(id).normal );

	% compute exterior unit normals of the edges
	mesh(id).nu1 = cross( mesh(id).tau1, mesh(id).normal );
	mesh(id).nu2 = cross( mesh(id).tau2, mesh(id).normal );
	mesh(id).nu3 = cross( mesh(id).tau3, mesh(id).normal );
	
	mesh(id).nu1 = mesh(id).nu1 / norm( mesh(id).nu1 );
	mesh(id).nu2 = mesh(id).nu2 / norm( mesh(id).nu2 );
	mesh(id).nu3 = mesh(id).nu3 / norm( mesh(id).nu3 );
end