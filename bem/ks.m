%KS  Solve linear eigenvalue problem.
%
%   [Y, L] = KS(AFUN, N) solves the linear eigenvalue problem A*x = lambda*x.
%   AFUN should be a handle to a function implementing the matrix-vector
%   product A*x and N is the size of the problem.  The output L has
%   orthonormal columns and satisfies A*Y = Y*L.  The problem is solved by
%   means of the Arnoldi algorithm with Kylov-Schur restarting.  The restarts
%   are performed in such a way that the reciprocal eigenvalues of L are
%   close to the real interval [-1, 1].

% Cedric Effenberger
% ETH Zurich, October 2011.
function [Y, L] = ks(Afun, n)

% determine basis sizes
base = 20;
expand = 20;

% define convergence tolerance
tolconv = 500*eps;
tolreal = 1e-1;

% initialization
locked = 0;
U = zeros(n, base+expand+1);
H = zeros(base+expand+1, base+expand);
start = 2;

% generate starting vector
U(:, 1) = randn(n, 1);
U(:, 1) = U(:, 1) / norm(U(:, 1), 2);

% main loop
while 1
	
	% expand basis
	for m = start:locked+base+expand+1

		% apply operator
		U(:, m) = feval(Afun, U(:, m-1));

		% orthogonalize
		H(1:m-1, m-1) = U(:, 1:m-1)' * U(:, m);
		U(:, m) = U(:, m) - U(:, 1:m-1)*H(1:m-1, m-1);

		% re-orthogonalize
		h = U(:, 1:m-1)' * U(:, m);
		H(1:m-1, m-1) = H(1:m-1, m-1) + h;
		U(:, m) = U(:, m) - U(:, 1:m-1)*h;

		% normalize
		H(m, m-1) = norm(U(:, m), 2);
		U(:, m) = U(:, m) / H(m, m-1);
	end
	
	% bring H to Schur form
	[Q, H(locked+1:locked+base+expand, locked+1:locked+base+expand)] = schur(H(locked+1:locked+base+expand, locked+1:locked+base+expand), 'complex');
	
	% compute Ritz values
	rv = 1 ./ ordeig(H(locked+1:locked+base+expand, locked+1:locked+base+expand));
	
	% purge unwanted Ritz values
	[ignore, idx] = sort(real(rv).^2 + (imag(rv)/tolreal).^2);
	sel = false(base+expand, 1);
	sel(idx(1:base)) = true;
	[Q, H(locked+1:locked+base+expand, locked+1:locked+base+expand)] = ordschur(Q, H(locked+1:locked+base+expand, locked+1:locked+base+expand), sel);
	
	% lock converged Ritz values
	H(locked+base+1, locked+1:locked+base) = H(locked+base+expand+1, locked+1:locked+base+expand) * Q(:, 1:base);
	newlylocked = 0;
	number = 1;
	while ( number <= base-newlylocked )
		
		% sort Ritz value into leading position
		sel = false(base-newlylocked, 1);
		sel(number) = true;
		[q, h] = ordschur(eye(base-newlylocked), H(locked+newlylocked+1:locked+base, locked+newlylocked+1:locked+base), sel);
		
		% check for convergence
		if ( abs(H(locked+base+1, locked+newlylocked+1:locked+base) * q(:, 1)) < tolconv )
			
			% lock Ritz value
			H(locked+base+1, locked+newlylocked+2:locked+base) = H(locked+base+1, locked+newlylocked+1:locked+base) * q(:, 2:end);
			H(locked+base+1, locked+newlylocked+1) = 0;
			Q(:, newlylocked+1:base) = Q(:, newlylocked+1:base) * q;
			H(locked+newlylocked+1:locked+base, locked+newlylocked+1:locked+base) = h;
			newlylocked = newlylocked + 1;
		else
			
			% increase counter
			number = number + 1;
		end
	end
	
	% test stopping criterion
	rv = 1 ./ diag(H(locked+1:locked+base, locked+1:locked+base));
	if ( (newlylocked > 0) && all( (abs(real(rv)) > 1) | (abs(imag(rv)) > tolreal) ) )
		
		% if satisfied, stop iterating
		break
	end
	
	% bring H back to Hessenberg form
	for m = newlylocked+2:base
		
		% introduce bulge
		G = H(locked+base+1, locked+m-1:locked+m);
		H(locked+base+1, locked+m-1) = 0;
		H(locked+base+1, locked+m) = norm(G, 2);
		G = [G(2), conj(G(1)); -G(1), conj(G(2))] / H(locked+base+1, locked+m);
		H(1:locked+m, locked+m-1:locked+m) = H(1:locked+m, locked+m-1:locked+m) * G;
		Q(:, m-1:m) = Q(:, m-1:m) * G;
		
		% chase bulge
		for k = m-2:-1:newlylocked+1
			
			H(locked+k+1:locked+k+2, locked+k:locked+base) = G' * H(locked+k+1:locked+k+2, locked+k:locked+base);
			G = H(locked+k+2, locked+k:locked+k+1);
			H(locked+k+2, locked+k) = 0;
			H(locked+k+2, locked+k+1) = norm(G, 2);
			G = [G(2), conj(G(1)); -G(1), conj(G(2))] / H(locked+k+2, locked+k+1);
			H(1:locked+k+1, locked+k:locked+k+1) = H(1:locked+k+1, locked+k:locked+k+1) * G;
			Q(:, k:k+1) = Q(:, k:k+1) * G;
		end
		H(locked+newlylocked+1:locked+newlylocked+2, locked+newlylocked+1:locked+base) = G' * H(locked+newlylocked+1:locked+newlylocked+2, locked+newlylocked+1:locked+base);
	end
	
	% apply transformations to basis
	U(:, locked+1:locked+base) = U(:, locked+1:locked+base+expand) * Q(:, 1:base);
	U(:, locked+base+1) = U(:, locked+base+expand+1);
	
	% update number of locked Ritz values
	start = locked+base+2;
	locked = locked + newlylocked;
end

% keep only desired eigenvalues
rv = 1 ./ diag(H(1:locked, 1:locked));
sel = (abs(real(rv)) <= 1) & (abs(imag(rv)) <= tolreal);
[Q, L] = ordschur(eye(locked), H(1:locked, 1:locked), sel);
sel = nnz(sel);
Y = U(:, 1:locked) * Q(:, 1:sel);
L = L(1:sel, 1:sel);