%GENMESH  Generate a boundary mesh for a specified geometry.
%
%   MESH = GENMESH(N, TYPE) returns a boundary mesh for the geometry
%   specified by the TYPE argument.  Supported options for TYPE are:
%
%      'UNITCUBE' - produces a boundary mesh for the unit cube [0,1]^3
%      'FICHERA'  - produces a boundary mesh for the Fichera corner
%                   [0,1]^3 \ [0.5, 1]^3
%
%   N controls the level of mesh refinement.  Larger N result in finer
%   meshes with more degrees of freedom.

% Cedric Effenberger
% ETH Zurich, October 2011.
function mesh = genmesh(N, type)

switch upper(type)
	
	case 'UNITCUBE'
		mesh = unitcubemesh(N);
	
	case 'FICHERA'
		mesh = ficheramesh(N);
	
	otherwise
		error('TYPE must be one of the following options:\n\t- UNITCUBE,\n\t- FICHERA.');
end