%CHEBCOL  Evaluate Chebyshev polynomials and their derivatives.
%
%   [TAU, DTAU] = CHEBCOL(LAMBDA, COL, DEG) applies the Chebyshev
%   polynomials of the first kind up to degree DEG to the matrix LAMBDA
%   and returns the columns with index COL of the results in TAU.
%   Moreover, the derivative of the first COL columns of the results
%   with respect to the column with index COL of LAMBDA is returned
%   in DTAU.

% Cedric Effenberger
% ETH Zurich, October 2011.
function [tau, Dtau] = chebcol(Lambda, col, deg)

% allocate memory for results
tau = zeros(size(Lambda, 1), deg+1);
Dtau = zeros(size(Lambda, 1), size(Lambda, 1)*col, deg+1);

% initialization for degrees 0 and 1
tau(col, 1) = 1;
tau(:, 2) = Lambda(:, col);

Dtau(:, end-size(Lambda, 1)+1:end, 2) = eye(size(Lambda));

% apply three-term recurrence of Chebyshev polynomials for higher degrees
for k = 2:deg
	
	tau(:, k+1) = 2*Lambda*tau(:, k) - tau(:, k-1);
	Dtau(:, :, k+1) = 2*(Lambda*Dtau(:, :, k) + kron(tau(1:col, k).', eye(size(Lambda)))) - Dtau(:, :, k-1);
end