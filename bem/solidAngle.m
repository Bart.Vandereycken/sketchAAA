%SOLIDANGLE  Compute the solid angles of a sequence of triangles.
%
%   SOLANG = SOLIDANGLE(P1, P2, P3) employs the Oosterom-Strackee-Algorithm [1]
%   to compute the solid angles with respect to the origin of the triangles
%   given by the vertices in P1, P2, and P3.
%
% [1] Van Oosterom, A.; Strackee, J. (1983). 
% "The Solid Angle of a Plane Triangle". 
% IEEE Trans. Biom. Eng BME-30 (2): 125–126. 
% doi:10.1109/TBME.1983.325207

% Cedric Effenberger and Michael Steinlechner
% ETH Zurich, October 2011.
function solang = solidAngle(P1,P2,P3)

numerator = abs(P1(1, :).*P2(2, :).*P3(3, :) - P1(1, :).*P2(3, :).*P3(2, :) ...
			  + P1(2, :).*P2(3, :).*P3(1, :) - P1(2, :).*P2(1, :).*P3(3, :) ...
			  + P1(3, :).*P2(1, :).*P3(2, :) - P1(3, :).*P2(2, :).*P3(1, :));

lP1 = sqrt(sum(P1.^2, 1));
lP2 = sqrt(sum(P2.^2, 1));
lP3 = sqrt(sum(P3.^2, 1));

denominator = lP1.*lP2.*lP3 + lP1.*sum(P2.*P3, 1) ...
			+ lP2.*sum(P1.*P3, 1) + lP3.*sum(P1.*P2, 1);

solang = 2*atan2(numerator, denominator);
idx = (solang < 0);
solang(idx) = solang(idx) + 2*pi;