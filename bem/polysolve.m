%POLYSOLVE  Solve polynomial eigenvalue problem.
%
%   [Y, L] = polysolve(P) sets up a linearization of the polynomial
%   eigenvalue problem specified by P and returns an invariant pair (Y, L)
%   of this linearization, representing the eigenvalues closest to the real
%   interval [-1, 1].  Considering an expansion of the polynomial in the
%   Chebyshev basis, P(:, :, k) specifies the coefficient matrix associated
%   with the k-th Chebyshev polynomial of the first kind.

% Cedric Effenberger
% ETH Zurich, October 2011.
function [Y, L] = polysolve(P)

% determine problem size
[nTri, nTri, d1] = size(P);

% set up linearization
A = [sparse([1:(d1-2)*nTri, nTri+1:(d1-2)*nTri], ...
	[nTri+1:(d1-1)*nTri, 1:(d1-3)*nTri], -1, (d1-2)*nTri, (d1-1)*nTri); ...
	reshape(P(:, :, 1:d1-3), nTri, []), ...
	P(:, :, d1-2) - P(:, :, d1), P(:, :, d1-1)];

% prefactorize matrix
[LA, UA, lperm, rperm, rowscale] = lu(A, 'vector');

% solve linearized eigenvalue problem
[Y, L] = ks(@applyAinvB, (d1-1)*nTri);

% invert eigenvalues
L = inv(L);

function u = applyAinvB(u)
u(nTri+1:end, :) = 2*u(nTri+1:end, :);
u(end-nTri+1:end, :) = P(:, :, end) * u(end-nTri+1:end, :);
u = rowscale \ -u;
u(rperm, :) = UA \ (LA \ u(lperm, :));
end

end