%UNITCUBEDEMO  Solve the Laplace eigenvalue problem on the unit cube
%by means of a boundary element formulation.
%
%   UNITCUBEDEMO performs the following steps:
%
%      - construction of a boundary mesh for the unit cube
%      - Chebyshev interpolation of the nonlinear matrix-valued function
%        resulting from the boundary formulation of the problem
%      - solution of the corresponding polynomial eigenvalue problem
%      - iterative refinement of the solution

% Cedric Effenberger
% ETH Zurich, October 2011.

% generate boundary mesh for unit cube
fprintf('Generating boundary mesh ...              '); tic
mesh = genmesh(4, 'unitcube');
fprintf('Done in %.6f seconds.\n\n', toc);

% compute Chebyshev approximation to boundary element discretization
fprintf('Constructing Chebyshev interpolant ...    '); tic
P = chebinterp([5, 12], 12, mesh, 3);
fprintf('Done in %.6f seconds.\n\n', toc);

% solve polynomial eigenvalue problem in Chebyshev basis by applying
% an Arnoldi method (Krylov-Schur algorithm) to a linearization
fprintf('Solving polynomial eigenvalue problem ... '); tic
[Y, L] = polysolve(P);
fprintf('Done in %.6f seconds.\n\n', toc);

% extract invariant pair for polynomial from (Y, L)
[X, L] = extractinvp(Y, L, P, 'firstblock');

% compute residual of obtained invariant pair
fprintf('Computing residual ...                    '); tic
R = evalchebpoly(P, X, L);
fprintf('Done in %.6f seconds.\n\n', toc);

% three steps of Newton-based iterative refinement
fprintf(['Performing iterative refinement\n\n', ...
	'#refinements    residual norm    comp. time (s)\n' ...
	'-----------------------------------------------\n', ...
	'           0    %13.6e\n'], norm(R, 'fro'));
for it = 1:3
	
	% refine invariant pair
	tic
	[X, L] = refineinvp(P, X, L);
	time = toc;
	
	% compute new residual
	R = evalchebpoly(P, X, L);
	fprintf('%12u    %13.6e    %14.6f\n', it, norm(R, 'fro'), time);
end

% extract eigenvalues from computed invariant pair
fprintf('\nThe following eigenvalues have been computed:\n');
lambda = extractev(L, [5, 12]);
fprintf('%15.6f %+.6ei\n', [real(lambda).'; imag(lambda).']);