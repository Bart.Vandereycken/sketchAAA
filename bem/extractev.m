%EXTRACTEV  Extract eigenvalues from invariant pair.
%
%   LAMBDA = EXTRACTEV(L, INTERVAL) extracts the eigenvalues from the
%   L-component of an invariant pair (X, L) and maps them affine linearly
%   to the given INTERVAL.

% Cedric Effenberger
% ETH Zurich, October 2011.
function lambda = extractev(L, interval)

% extract eigenvalues from L and transform to given interval
lambda = 0.5 * (interval(2) - interval(1)) * eig(L) ...
	   + 0.5 * (interval(1) + interval(2));