%TRANSFORML  Transform invariant pair back to interval of interest.
%
%   L = TRANSFORML(L, INTERVAL) applies an affine linear map to the
%   L-component of an invariant pair (X, L), mapping the interval [-1, 1]
%   onto the given INTERVAL.

% Cedric Effenberger
% ETH Zurich, October 2011.
function L = transformL(L, interval)

% transform L
L = 0.5 * (interval(2) - interval(1)) * L ...
	+ 0.5 * (interval(1) + interval(2)) * eye(size(L));