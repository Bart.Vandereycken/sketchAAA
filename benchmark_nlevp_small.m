function benchmark_nlevp_small()
% Benchmark problems from nlevp that are small enough so
% that F(Z) fits in memory.

% Takes about 15min. 
%
% Results of the computation (including timings) are stored in
% the folder save_dir as MATLAB tables. 

% BV - 11.2022

save_dir = 'result_benchmarks_11_2022/';

kps = 1:23;
tols = [1e-8 1e-12];
Nsamples = 10; % number of random runs per surrogate

tab_summ = {}; % the final table with the summary of the calculation 
tab_make_sketch = {};

for kp = kps
    % F(z) = sum_i funf(z)
    [F, n, pname, Z, ~, ~, funf] = get_nlevp_problem(kp);  
    % 8 byte per double
    % suppose machine with 64GB
    % FZ can be at most 8GB for safety
    % 4 GB = 1e9 bytes 
    % kp = 17 (FZ of size 1410^2 x 400 = 6.3GB) takes in total 87GB on
    % server! -> Killed because spdiags in sv_aaa_orig takes a lot of mem!    
    if n*n*length(Z)*8 > 1e9 % (1 GB max)
        warning(sprintf('problem %d is too big for explicitly constructing FZ', kp))
        
        times_FZ(kp) = NaN;
    else
        name_problem = sprintf('nlevp_%02i_%s', kp, pname);
        name_problem

        % construct FZ is n^2 x M        
        T = tic();
        FZ = zeros(n*n, length(Z));
        for i = 1:length(Z)
            FZi = F(Z(i));
            FZ(:,i) = FZi(:);       
        end
        time_FZ = toc(T);

         % construct fZ is r x M        
        T = tic();
        fZ = zeros(length(funf(Z(1))), length(Z));
        for i = 1:length(Z)
            fZi = funf(Z(i));
            fZ(:,i) = fZi(:);       
        end
        time_fZ = toc(T);
        
        % compute the problem kp for each tol 
        for tol=tols            
            [tab_summ_kp, tab_make_sketch_kp] = run_one_problem(FZ, fZ, Z, F, tol, kp, Nsamples, strcat(save_dir, 'all/'));
                                        
            tab_summ = [tab_summ; tab_summ_kp];
            tab_make_sketch = [tab_make_sketch; tab_make_sketch_kp];
        end
        times_FZ(kp) = time_FZ;
        times_fZ(kp) = time_fZ;
    end
    
end

% table with timings to construct FZ and fZ
tab_FZ = table(kps, times_FZ, times_fZ, 'VariableNames', {'kp', 'time_FZ', 'time_fZ'});

% save tables
savefile = sprintf('table_benchmark_sv_small.mat');
save(strcat(save_dir,savefile), 'tab_summ', 'tab_make_sketch', 'tab_FZ');

tab_summ
times_FZ

end

function [tab_summ, tab_make_sketch] = run_one_problem(FZ, fZ, Z, F, tol_inf_norm, kp, Nr, save_dir)
% benchmark one problem for many values of ell

norm_inf_F = max(abs(FZ(:)));
norm_l2_F = sqrt(max(sum(conj(FZ).*FZ,2)));

max_deg = 50;

% Table with all the calculation output
tab_all = table('Size',[0 10], 'VariableNames', {'kp'  , 'tol_inf', 'rng' , 'svd_up' , 'method', 'time'  , 'deg'   , 'err_g' , 'err_F_l2', 'err_F_inf'}, ...
                               'VariableTypes', {'int8', 'double' , 'int8', 'logical', 'string', 'double', 'double', 'double', 'double'  , 'double'   });

%% Original SV_AAA (with svd updating) on vector f 
T=tic();
[r, pol, res, zer, z, f, w, errvec] = sv_aaa(fZ.', Z, 'mmax', max_deg+1, 'tol', tol_inf_norm, 'scale_each_F', true);
time = toc(T);
% Define vector-valued rational approximation r for F based on
% barycentric points and weights z,w (computed using f).
FZ_for_f = zeros(size(FZ,1), length(z));
for i = 1:length(z)
    FZi = F(z(i));
    FZ_for_f(:,i) = FZi(:);       
end
R = @(zz) eval_bary_vec(zz, z, w, FZ_for_f);

[norm_inf_err, norm_l2_err] = compute_errors(FZ, R(Z));

tab_all = [tab_all; {kp,tol_inf_norm,0,true,'orig_f',time,length(z),errvec(end),norm_l2_err/norm_l2_F,norm_inf_err/norm_inf_F}];            


%% Original SV_AAA (with svd updating) on whole F(z) - produces the same results as new code below with ell=0 
T=tic();
[r, pol, res, zer, z, f, w, errvec] = sv_aaa(FZ.', Z, 'mmax', max_deg+1, 'tol', tol_inf_norm, 'scale_each_F', false);
time = toc(T);
[norm_inf_err, norm_l2_err] = compute_errors(FZ, r(Z).');

tab_all = [tab_all; {kp,tol_inf_norm,0,true,'orig_F',time,length(z),errvec(end),norm_l2_err/norm_l2_F,norm_inf_err/norm_inf_F}];            


%% Sketched AAA - Nr runs
opts.tensor_sketch = false;
opts.max_norm = true;
opts.tol = tol_inf_norm;
opts.deg = max_deg;
opts.error_on_F = false;

tab_make_sketch = table('Size',[0 3], 'VariableNames', {'rng' , 'ell' , 'time'  }, ...
                                      'VariableTypes', {'int8', 'int8', 'double'});
ells = [0 1 4 8 12];
rngs = 1:Nr;
svd_ups = [false true];


N = size(FZ,1);
for ell = ells
    ell
    for i_rng = rngs
        
        if ell==0
            GZ = FZ;
            time_sketch = 0;
        else            
            T=tic();
            rng(i_rng)  
            UV = zeros(N,ell);            
            for i=1:ell
                v = randn(N,1); v = v/norm(v);
                UV(:,i) = v;            
            end          
            GZ = UV'*FZ;           
            time_sketch=toc(T);
            tab_make_sketch = [tab_make_sketch; {i_rng,ell,time_sketch}];
        end


        for svd_up = svd_ups
            skip = false;
            if ell==0 
                if ~svd_up  && size(FZ,1)> 15^2 % skip if no svd update and large problem; otherwise takes too long 
                    skip = true;
                end
                if length(rngs)>=4 && i_rng>rngs(4) % 
                    skip = true;
                end
            end  
            if ~skip
                opts.svd_up = svd_up;
                T=tic();
                [z,w,ind,stats_faster] = sketchAAA(GZ,Z,max_deg,opts);
                R = @(zz) eval_bary_vec(zz, z, w, FZ(:,ind));
                time = toc(T) + time_sketch;
                
                [norm_inf_err, norm_l2_err] = compute_errors(FZ,R(Z));
                tab_all = [tab_all; {kp,tol_inf_norm,i_rng,svd_up,sprintf('ell=%02i',ell),time,length(z),stats_faster.err_g(end),norm_l2_err/norm_l2_F,norm_inf_err/norm_inf_F}];            
            end
        end
    end
end

tab_make_sketch_sum = groupsummary(tab_make_sketch,{'ell'},{"mean", "std"} ,{'time'});
tab_summ = groupsummary(tab_all,{'kp', 'tol_inf', 'svd_up', 'method'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});

% tab_orig
tab_make_sketch_sum
tab_summ

opts = rmfield(opts, {'svd_up'});

savefile = sprintf('table_benchmark_sv_small_%i_tol%.1e.mat', kp, tol_inf_norm);
save(strcat(save_dir,savefile), 'tab_all', 'opts')

end

function [maxerr, L2err] = compute_errors(F,R)
% F is matrix with F evaluations
% F(:,i) is error of F(Z(i))
E = F-R;
maxerr = max(abs(E(:)));
L2err = sqrt(max(sum(conj(E).*E,2)));
end


