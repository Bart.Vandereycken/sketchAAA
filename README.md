# sketchAAA 

MATLAB software that implements the ``sketchAAA`` algorithm from the Arxiv preprint [Randomized sketching of nonlinear eigenvalue problems](https://arxiv.org/abs/2211.12175).

## Installation and usage

Download [release 1.0](https://gitlab.unige.ch/Bart.Vandereycken/sketchAAA/-/releases/v1.0) or clone the repository from one of the links on the top of this page. There is no particular installation needed in MATLAB since all files are included. 

The file ``example.m`` is a simple example on how to use ``sketchAAA.m``. You should see the following error plot

![Error example](figures/error_example.png)

For more advanced uses with expensive nonlinear eigenvalue problems, see the ``benchmark_*.m`` files.

## Reproducing numerical experiments

All numerical experiments from the above preprint can be reproduced using the scripts ``benchmark_*.m`` and ``make_table_*.m``.

The actual tables in the preprint were obtained using version 1.0 of the software.

## Credits

The code is based on joint work by Stefan Güttel, Daniel Kressner, and Bart Vandereycken.

This software is GPL licensed. When using it for academic purposes, please cite the following preprint:

```
@article{
    title = {Randomized sketching of nonlinear eigenvalue problems},
    journal = {arXiv:2211.12175},
    author = {G\"uttel, Stefan and Kressner, Daniel and Vandereycken, Bart},
    doi = {10.48550/arXiv.2211.12175},
    year = {2022}, 
}
```
This work was partly supported by the Swiss National Science Foundation under research project 192129.

