Problem supplied by Davide Pradovera.
See also Model order reduction based on functional rational approximants for parametric PDEs
with meromorphic structure, PhD thesis, EPFL, 2021.

The scattering problem reads
	(K - i * z * C - z^2 * M) * v(z) = b .
A sample solve is carried out in `solve.m`.

- v is a univariate vector-valued function, taking values in \C^20054. Its dependence on z is rational. The poles of v are located in the bottom half-complex plane (negative imaginary part).

- K, C, and M are non-symmetric real matrices of size 20054. K (stiffness) is non-singular. C (boundary damping) and M (mass) are singular.

- b is a real vector of size 20054. Notably, b is the 5305-th element of the canonical basis (stored in sparse format for convenience).

- z is the wavenumber, which is real for lossless materials.

v contains the nodal DoFs of the FE solution of a scattering problem over the unit disk. The wave propagates according to the Helmholtz equation with absorbing boundary conditions, from a point source located approximately at (-0.203, 0.). The scatterer is a disk of radius .2 and contrast 5. A sample solution for z = 7 is shown in `sol.png`.

For 5 <= z <= 10, a plot of the (Euclidean) norm of v is shown in `norm.png`. There are no poles on the real axis, but some are fairly close to it. (They are "almost-resonances" due to the wave bouncing around inside the scatterer.) In `poles.png`, some of the poles approximated via MRI are shown.

