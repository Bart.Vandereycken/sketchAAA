load('K.mat'), load('C.mat'), load('M.mat'), load('b.mat')
b = full(b);

z = 7;
v = ( K - 1j * z * C - z^2 * M ) \ b;
v_norm = norm(v)
