% example on how to use sketchAAA

% random problem
rng(1); n = 7;
B = randn(n); C = randn(n);
F = @(x)( abs(x)*B + sin(pi*x)*C );
Z = linspace(-1,1,200); 

% evaluate vectorized F on Z
FZ = zeros(n*n, length(Z));
for i=1:length(Z)
    FZi = F(Z(i)); FZ(:,i) = FZi(:);
end

% construct 4 surrogates 
ell = 4;  UV = randn(n*n,ell);
GZ = UV'*FZ;           

% compute barycentric form of AAA approximation R
max_deg = 30; opts.tol = 1e-6;
[z,w,ind,stats_faster] = sketchAAA(GZ,Z,max_deg, opts);
R = @(zz) eval_bary_vec(zz, z, w, FZ(:,ind));

% compute max error wrt Z
E = R(Z) - FZ;
e = vecnorm(E,'inf');
max_err = max(e)

% plot error wrt Z, lines indicate the support points (with zero error)
semilogy(Z,e,'-', 'Linewidth', 2)
hold on
xline(z,'k-')
xlabel('z')
ylabel('max error')
