% Make a latex snippet to report the experiment as done in the paper.
% Ready for copy paste with some minor formatting afterwards.

load('result_benchmarks_11_2022/table_benchmark_sv_large.mat')

% Choose manually:
% TOL = 1e-08;
TOL = 1e-12;
% TENSOR = true;
TENSOR = false;

% Generate table rows per tolerance and per tensor flag.
% For all problems, we first give the stats without split form.
% For the last problem (here 24) the split form stats will also be added.

% filter to a smaller dataset
T = tab_summ;
rows = (T.tol_inf==TOL) & (T.svd_up==true) & ismember(T.method,["ell=00","ell=01","ell=04","orig_f"]);
T = T(rows,:);
cols = ["kp", "method", "mean_time", "mean_deg", "mean_err_F_inf", "tensor", "split"];
T = T(:,cols);
T

% start latex row generation
latex = "|begin{tabular}{lcccccc}";
line1 = sprintf("%0.1e ", TOL);
latex = sprintf("%s \\n %s ", latex, line1);        


% first all problems, not split
ks = sort(double(unique(T.kp)), 'ascend');
for ik = 1:length(ks)
    k = ks(ik);

    line1 = sprintf(" & %d  & ", k);
    line2 = sprintf(" &   & ");

    % This is SV AAA on f (vector of scalar-valued functions)        
    t_d_e = table2array( T((T.kp==k) & (T.method=="orig_f"), ["mean_time", "mean_deg", "mean_err_F_inf"]) );    
    line1 = sprintf(" %s     %2g  &  %0.1e", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s    |multicolumn{2}{c}{ %0.3f } ", line2, t_d_e(1));
    
    
    % This is sketchedAAA on F with 1 surrogate - not split
    t_d_e = table2array( T((T.kp==k) & (T.method=="ell=01") & (T.tensor==TENSOR) & (T.split==false), ["mean_time", "mean_deg", "mean_err_F_inf"]) );
    line1 = sprintf(" %s  &  %2g  &  %0.1e ", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s  &  |multicolumn{2}{c}{ %0.3f } ", line2, t_d_e(1));

    % This is sketchedAAA on F with 4 surrogates
    t_d_e = table2array( T((T.kp==k) & (T.method=="ell=04") & (T.tensor==TENSOR) & (T.split==false), ["mean_time", "mean_deg", "mean_err_F_inf"]) );
    line1 = sprintf(" %s  &  %2g  &  %0.1e || ", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s  &  |multicolumn{2}{c}{ %0.3f } || ", line2, t_d_e(1));

    latex = sprintf("%s \\n %s \\n %s", latex, line1, line2);        
end

% last, add final problem with split - basically same code as above but
% with T.split=true
k = ks(end);
    line2 = sprintf(" & %d*  & ", k);    

    % This is SV AAA on f (vector of scalar-valued functions)        
    t_d_e = table2array( T((T.kp==k) & (T.method=="orig_f"), ["mean_time", "mean_deg", "mean_err_F_inf"]) );    
    line1 = sprintf(" %s     %2g  &  %0.1e", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s          &    ", line2);
    
    
    % This is sketchedAAA on F with 1 surrogate - not split
    t_d_e = table2array( T((T.kp==k) & (T.method=="ell=01") & (T.tensor==TENSOR) & (T.split==true), ["mean_time", "mean_deg", "mean_err_F_inf"]) );    
    line2 = sprintf(" %s  &  |multicolumn{2}{c}{ %0.3f } ", line2, t_d_e(1));

    % This is sketchedAAA on F with 4 surrogates
    t_d_e = table2array( T((T.kp==k) & (T.method=="ell=04") & (T.tensor==TENSOR) & (T.split==true), ["mean_time", "mean_deg", "mean_err_F_inf"]) );    
    line2 = sprintf(" %s  &  |multicolumn{2}{c}{ %0.3f } || ", line2, t_d_e(1));

    latex = sprintf("%s \\n %s", latex, line2);

line = "|end{tabular}";
latex = sprintf("%s \\n %s \n", latex, line);

% latex uses \\ but fprintf does like it, we use | instead above and
% replace at the end
fprintf(strrep(latex,"|", "\\"))