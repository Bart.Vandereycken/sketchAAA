% Make a latex snippet to report the experiment as done in the paper.
% Ready for copy paste with some minor formatting afterwards.

% Load files seperately since each computation takes a long time (days).
load('result_benchmarks_11_2022/all/table_bem_large_level03_tol1.0e-08.mat')
tab_summ = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
load('result_benchmarks_11_2022/all/table_bem_large_level03_tol1.0e-12.mat')
tab_summ_kp = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
tab_summ = [tab_summ; tab_summ_kp];
load('result_benchmarks_11_2022/all/table_bem_large_level05_tol1.0e-08.mat')
tab_summ_kp = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
tab_summ = [tab_summ; tab_summ_kp];
load('result_benchmarks_11_2022/all/table_bem_large_level05_tol1.0e-12.mat')
tab_summ_kp = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
tab_summ = [tab_summ; tab_summ_kp];
load('result_benchmarks_11_2022/all/table_bem_large_level07_tol1.0e-08.mat')
tab_summ_kp = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
tab_summ = [tab_summ; tab_summ_kp];
load('result_benchmarks_11_2022/all/table_bem_large_level07_tol1.0e-12.mat')
tab_summ_kp = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
tab_summ = [tab_summ; tab_summ_kp];
load('result_benchmarks_11_2022/all/table_bem_large_level09_tol1.0e-08.mat')
tab_summ_kp = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
tab_summ = [tab_summ; tab_summ_kp];
load('result_benchmarks_11_2022/all/table_bem_large_level09_tol1.0e-12.mat')
tab_summ_kp = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});
tab_summ = [tab_summ; tab_summ_kp];

%%%

% Choose manually:
TOL = 1e-12;
LEVEL  = 3;
% TENSOR = false;
TENSOR = true;

T = tab_summ;
T(T.tol_inf==TOL,:)


rows = (T.tol_inf==TOL) & (T.svd_up==true) & (T.level==LEVEL) & (T.tensor==TENSOR) ;
T = T(rows,:);

cols = ["method", "mean_time", "mean_deg", "mean_err_F_inf", "tensor"];
T = T(:,cols);

table_size = size(T) ; 
rows = table_size(1); 
line = "";
for row = 1:rows 
    m_t_d_e = table2array(T(row,:));
    ell = extractAfter(m_t_d_e(1),"ell=");
    line = sprintf(" %s \\n  %s  &  %0.3f & %0.1f  &  %0.1e || ", line, ell, m_t_d_e(2), m_t_d_e(3), m_t_d_e(4));
end 

fprintf(strrep(line,"|", "\\"))



