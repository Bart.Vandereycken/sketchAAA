function nice_example(compute)
% Nice motivating example at start of paper showing that already two
% surrogates is a dramatic improvement.

if compute
    do_compute(2, [1 2])
    do_compute(10, [1 2 3 4])
end



nice_cols = plot_defaults();

%%% Plot one run (r=1) of buckling plate
load('results/nice_example_results_kp2.mat')

r = 1; d = 1:2:50;
figure(1)
semilogy(d, err_F_ten{1}(r,d), 'o-', 'Color', nice_cols(1,:))
hold on
semilogy(d, err_F_ten{2}(r,d), 'o-', 'Color', nice_cols(3,:))
text(23, 1e-10, '$\ell=1$', 'FontSize', 22)

semilogy(d, err_F_noten{1}(r,d), 'x-', 'Color', nice_cols(1,:))
semilogy(d, err_F_noten{2}(r,d), 'x-', 'Color', nice_cols(3,:))
text(30, 1e-2, '$\ell=2$', 'FontSize', 22)

semilogy(d, err_F(d), '*', 'Color', [0.25, 0.25, 0.25], 'MarkerSize',12, 'MarkerEdgeColor',[0.25, 0.25, 0.25], 'MarkerFaceColor',[0.25, 0.25, 0.25])
xlabel('degree $d$'); ylabel('$\| F - R \|_\Sigma$')
title('Buckling plate: error wrt function $F$')
hl = legend('$\ell=1$, tens.', '$\ell=2$, tens.', '$\ell=1$, full', '$\ell=2$, full', 'SV-AAA', 'Location','NE');
set(hl, 'Interpreter','latex');
saveas(gca, 'figures/nice_example_F.eps','epsc')


figure(2)
semilogy(d, err_g_ten{1}(r,d), 'ro-', 'Color', nice_cols(1,:))
hold on
semilogy(d, err_g_ten{2}(r,d), 'bo-', 'Color', nice_cols(3,:))
text(18, 1e-11, '$\ell=1$', 'FontSize', 22)

semilogy(d, err_g_noten{1}(r,d), 'rx-', 'Color', nice_cols(1,:))
semilogy(d, err_g_noten{2}(r,d), 'bx-', 'Color', nice_cols(3,:))
text(25, 1e-7, '$\ell=2$', 'FontSize', 22)

xlabel('degree $d$');  ylabel('$\| g - r \|_\Sigma$')
title('Buckling plate: error wrt surrogate $g$')
hl = legend('$\ell=1$, tens.', '$\ell=2$, tens.', '$\ell=1$, full', '$\ell=2$, full', 'Location','NE');
set(hl, 'Interpreter','latex');
saveas(gca, 'figures/nice_example_g.eps','epsc')


%%% Plot percentiles of buckling plate
load('results/nice_example_results_kp2.mat')
min_err = min(err_F);


figure(3)
plot_conf_band(degs, err_F_noten{1}, nice_cols(1,:), '-', 'n')
hold on
text(23, 1e-10, '$\ell=1$', 'FontSize', 22)
plot_conf_band(degs, err_F_noten{2}, nice_cols(3,:), '-', 'n')
text(30, 1e-2, '$\ell=2$', 'FontSize', 22)
semilogy([degs(1) degs(end)], [min_err min_err], '--', 'Color', [0.25, 0.25, 0.25])

xlabel('degree $d$');  ylabel('$\| F - R \|_\Sigma$')
title('Buckling plate: 90 and 50 percentiles of error')
hl = legend('$\ell=1$', '$\ell=2$', 'Location','NE');
set(hl, 'Interpreter','latex');
saveas(gca, 'figures/nice_example_F_pct.eps','epsc')

%%% Plot percentiles of nep2
load('results/nice_example_results_kp10.mat')
min_err = min(err_F);

figure(4)
ells
plot_conf_band(degs, err_F_noten{1}, nice_cols(1,:), '-', 'n')
hold on
text(20, 1e-2, '$\ell=1$', 'FontSize', 22)
plot_conf_band(degs, err_F_noten{2}, nice_cols(3,:), '-', 'n')
text(30, 5e-6, '$\ell=2$', 'FontSize', 22)
plot_conf_band(degs, err_F_noten{4}, nice_cols(5,:), '-', 'n')
text(40, 1e-10, '$\ell=4$', 'FontSize', 22)
semilogy([degs(1) degs(end)], [min_err min_err], '--', 'Color', [0.25, 0.25, 0.25])

xlabel('degree $d$');  ylabel('$\| F - R \|_\Sigma$')
title('Nep2: 90 and 50 percentiles of error')
hl = legend('$\ell=1$', '$\ell=2$', '$\ell=4$', 'Location','NE');
set(hl, 'Interpreter','latex');
saveas(gca, 'figures/nice_example_F_pct_nep2.eps','epsc')


end

%%%%%%%%%%%%
function plot_conf_band(x_data, errors, color, ls, name)
    % percentiles are more robust than std error conf intervals
    % https://ch.mathworks.com/matlabcentral/answers/159417-how-to-calculate-the-confidence-interval
    CIFcn = @(x,p)prctile(x,abs([0,100]-(100-p)/2));
    
    m_e = median(errors);
    semilogy(x_data, m_e, ls, 'Linewidth', 2, 'Color', color, 'DisplayName', name)
    hold on
    ci_e = CIFcn(errors,90);            
    p = patch([x_data, fliplr(x_data)], [ci_e(1,:), fliplr(ci_e(2,:))], color, 'FaceAlpha',0.3, 'LineStyle','none');    
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');    

    ci_e = CIFcn(errors,50);            
    p = patch([x_data, fliplr(x_data)], [ci_e(1,:), fliplr(ci_e(2,:))], color, 'FaceAlpha',0.6, 'LineStyle','none');    
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');    

%     for i=9:-0.5:1
%         ci_e = CIFcn(errors,i*10);            
%         p = patch([x_data, fliplr(x_data)], [ci_e(1,:), fliplr(ci_e(2,:))], color, 'FaceAlpha',1-0.1*i, 'LineStyle','none');    
%         set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');    
%     end

    % semilogy(x_data, ci_e(1,:), '--', 'Linewidth', 1, 'Color', color)
    % semilogy(x_data, ci_e(2,:), '--', 'Linewidth', 1, 'Color', color)
end


function do_compute(kp, ells)
% Very simple implementation (each new degree is calculated from scratch).
% Different sizes of the sketch are in ells
degs = 1:50; % degrees to test

rng_seed = 0; 
nb_runs = 100;

opts.max_norm = true;
opts.tol = 0;
opts.error_on_F = false;


% get eigenvalue problem F(z), Z, and surrogate function f(z)
[F, n, pname, Z, ~, ~] = get_nlevp_problem(kp);
pname
T = tic();
FZ = zeros(n*n, length(Z));
for i = 1:length(Z)
    FZi = F(Z(i));
    FZ(:,i) = FZi(:);       
end
time_FZ = toc(T);

N = size(FZ,1);

%%% Compare ell surrogates. Do multiple runs.
nb_degs = length(degs);
nb_ells = length(ells);
err_F = zeros(1,nb_degs);

for il = 1:nb_ells
    il
    ell = ells(il);    
    err_F_ten{il} = zeros(nb_runs,nb_degs);
    err_F_noten{il} = zeros(nb_runs,nb_degs);
    err_g_ten{il} = zeros(nb_runs,nb_degs);
    err_g_noten{il} = zeros(nb_runs,nb_degs);


    for id = 1:nb_degs        
        d = degs(id);        
        for r = 1:nb_runs            
            % Tensorized sketch, size 1 and 2            
            rng(rng_seed+r)  
            UV = zeros(N,ell);            
            for i=1:ell
                v = kron(randn(sqrt(N),1),randn(sqrt(N),1)); v = v/norm(v);
                UV(:,i) = v;            
            end          
            GZ = UV'*FZ;                       
            
            [z,w,ind,stats] = sketchAAA(GZ,Z,d,opts);
            R = @(zz) eval_bary_vec(zz, z, w, FZ(:,ind));            
            [norm_inf_err, norm_l2_err] = compute_errors(FZ,R(Z));
            err_F_ten{il}(r,id) = norm_inf_err;
            err_g_ten{il}(r,id) = stats.err_g(end);            
                
            % Full sketch, size 1 and 2            
            rng(rng_seed+r)  
            UV = zeros(N,ell);            
            for i=1:ell
                v = randn(N,1); v = v/norm(v);
                UV(:,i) = v;            
            end          
            GZ = UV'*FZ; 

            [z,w,ind,stats] = sketchAAA(GZ,Z,d,opts);
            R = @(zz) eval_bary_vec(zz, z, w, FZ(:,ind));            
            [norm_inf_err, norm_l2_err] = compute_errors(FZ,R(Z));
            err_F_noten{il}(r,id) = norm_inf_err;
            err_g_noten{il}(r,id) = stats.err_g(end);           
            
        end

        % Set-valued AAA of F
        GZ = FZ;
        [z,w,ind,stats] = sketchAAA(GZ,Z,d,opts);
        R = @(zz) eval_bary_vec(zz, z, w, FZ(:,ind));            
        [norm_inf_err, norm_l2_err] = compute_errors(FZ,R(Z));
        err_F(1,id) = norm_inf_err;
        

%         [R,err_F_d,t_calc_d] = aaa_sv(F,Z,d);
%         err_F(1,id) = err_F_d;
%         t_calc_sv(1,id) = t_calc_d;
    end
       
end

save(sprintf('results/nice_example_results_kp%i.mat', kp))

end

function [maxerr, L2err] = compute_errors(F,R)
% F is matrix with F evaluations
% F(:,i) is error of F(Z(i))
E = F-R;
maxerr = max(abs(E(:)));
L2err = sqrt(max(sum(conj(E).*E,2)));
end


