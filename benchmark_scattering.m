function benchmark_scattering()
% Benchmark scattering problem. Not in split form but can fit in memory.
 
% It is advisable to run this on a computing node with 64 GB RAM. 
% Takes about 10min on a beefy multicore machine.

% Results of the computation (including timings) are stored in
% the folder save_dir as MATLAB tables. 

% BV - 11.2022

save_dir = 'result_benchmarks_11_2022/';

tols = [1e-8 1e-12];
Nsamples = 10; % number of random runs per surrogate


tab_summ = {}; % the final table with the summary of the calculation 
tab_make_sketch = {};


[F, N, pname, Z] = get_scattering_problem();

% construct FZ is n^2 x M        
T = tic();
FZ = F(Z);        
time_FZ = toc(T);

% compute the problem kp for each tol 
for tol=tols            
    [tab_summ_kp, tab_make_sketch_kp] = run_one_problem(FZ, Z, F, tol, Nsamples, strcat(save_dir, 'all/'));
                                
    tab_summ = [tab_summ; tab_summ_kp];
    tab_make_sketch = [tab_make_sketch; tab_make_sketch_kp];
end
times_FZ = time_FZ;        
   
% table with timings to construct FZ and fZ
tab_FZ = table(times_FZ, 'VariableNames', {'time_FZ'});

% save tables
savefile = sprintf('table_benchmark_scattering.mat');
save(strcat(save_dir,savefile), 'tab_summ', 'tab_make_sketch', 'tab_FZ');

tab_summ
times_FZ

end

function [tab_summ, tab_make_sketch] = run_one_problem(FZ, Z, F, tol_inf_norm, Nr, save_dir)
% benchmark one problem for many values of ell

norm_inf_F = max(abs(FZ(:)));
norm_l2_F = sqrt(max(sum(conj(FZ).*FZ,2)));

max_deg = 50;

% Table with all the calculation output
tab_all = table('Size',[0 9], 'VariableNames', {'tol_inf', 'rng' , 'svd_up' , 'method', 'time'  , 'deg'   , 'err_g' , 'err_F_l2', 'err_F_inf'}, ...
                               'VariableTypes', {'double' , 'int8', 'logical', 'string', 'double', 'double', 'double', 'double'  , 'double'   });


% skip SV-AAA F       

%% Sketched AAA - Nr runs
opts.tensor_sketch = false;
opts.max_norm = true;
opts.tol = tol_inf_norm;
opts.deg = max_deg;
opts.error_on_F = false;

tab_make_sketch = table('Size',[0 3], 'VariableNames', {'rng' , 'ell' , 'time'  }, ...
                                      'VariableTypes', {'int8', 'int8', 'double'});
ells = [0 1 4 8 16 24];
rngs = 1:Nr;
svd_ups = [true];


N = size(FZ,1);
for ell = ells
    ell
    for i_rng = rngs
        
        if ell==0
            GZ = FZ;
            time_sketch = 0;
        else
            T=tic();
            rng(i_rng)  
            UV = zeros(N,ell);            
            for i=1:ell
                v = randn(N,1); v = v/norm(v);
                UV(:,i) = v;            
            end          
            GZ = UV'*FZ;           
            time_sketch=toc(T);
            tab_make_sketch = [tab_make_sketch; {i_rng,ell,time_sketch}];
        end


        for svd_up = svd_ups
            skip = false;
            if ell==0 
                if ~svd_up  && size(FZ,1)> 15^2 % skip if no svd update and large problem; otherwise takes too long 
                    skip = true;
                end
                if length(rngs)>=4 && i_rng>rngs(4) % ?? BV 09.2022: Not sure why ?? not randomized so only 3 runs to get timing estimate
                    skip = true;
                end
            end  
            if ~skip
                opts.svd_up = svd_up;
                T=tic();
                [z,w,ind,stats_faster] = sketchAAA(GZ,Z,max_deg,opts);
                R = @(zz) eval_bary_vec(zz, z, w, FZ(:,ind));
                time = toc(T) + time_sketch;
                [norm_inf_err, norm_l2_err] = compute_errors(FZ,R(Z));
                tab_all = [tab_all; {tol_inf_norm,i_rng,svd_up,sprintf('ell=%02i',ell),time,length(z),stats_faster.err_g(end),norm_l2_err/norm_l2_F,norm_inf_err/norm_inf_F}];            
            end
        end
    end
end

tab_make_sketch_sum = groupsummary(tab_make_sketch,{'ell'},{"mean", "std"} ,{'time'});
tab_summ = groupsummary(tab_all,{'tol_inf', 'svd_up', 'method'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});

% tab_orig
tab_make_sketch_sum
tab_summ

opts = rmfield(opts, {'svd_up'});

savefile = sprintf('table_benchmark_scattering_tol%.1e.mat', tol_inf_norm);
save(strcat(save_dir,savefile), 'tab_all', 'opts')

savefile = sprintf('R_scattering_ell%02i_tol%.1e.mat', ell, tol_inf_norm);
save(strcat(save_dir,savefile), 'R')


end

function [maxerr, L2err] = compute_errors(F,R)
% F is matrix with F evaluations
% F(:,i) is error of F(Z(i))
E = F-R;
maxerr = max(abs(E(:)));
L2err = sqrt(max(sum(conj(E).*E,2)));
end


