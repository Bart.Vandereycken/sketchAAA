function benchmark_bem_large()
% Benchmark scattering problem. Not in split form but cannot fit in memory.

% It is advisable to run this on a computing node with 64 GB RAM.
% Total time is several days, even on a beefy multicore machine, since
% all problems are computed for 10 random realizations.
%
% Results of the computation (including timings) are stored in
% the folder save_dir as MATLAB tables. 

% BV - 11.2022

save_dir = 'result_benchmarks_11_2022/';

tols = [1e-8 1e-12];
Nsamples = 10; % number of random runs per surrogate


tab_summ = {}; % the final table with the summary of the calculation 
tab_make_sketch = {};

meshlevels = [3 5 7 9];  % large problems where we do not store the whole matrix 


for meshlevel=meshlevels
    [F, pname, N, Z] = get_bem_problem(meshlevel, 3);

    % compute the problem with meshlevel for each tol 
    for tol=tols            
        [tab_summ_kp, tab_make_sketch_kp] = run_one_problem(F, Z, meshlevel, tol, Nsamples, strcat(save_dir, 'all/'));
                                    
        tab_summ = [tab_summ; tab_summ_kp];
        tab_make_sketch = [tab_make_sketch; tab_make_sketch_kp];

        % save tables
        savefile = sprintf('table_benchmark_bem_large.mat');
        save(strcat(save_dir,savefile), 'tab_summ', 'tab_make_sketch', 'tab_FZ');

    end        
end    

tab_summ
times_FZ

end

function [tab_summ, tab_make_sketch] = run_one_problem(Fvec, Z, meshlevel, tol_inf_norm, Nr, save_dir)
% benchmark one problem for many values of ell

% evaluating F is much cheaper for many z compared to evaluating one at a time
% however, we cannot afford to evaluate all points in Z due to memory
% constraints
WINDOW_Z = 20; 
nb_eval = floor(length(Z)/WINDOW_Z) + rem(length(Z),WINDOW_Z);
norm_inf_F = zeros(nb_eval,1);
norm_l2_F = zeros(nb_eval,1);
i_start = 1;
i_end = min(i_start+WINDOW_Z-1,length(Z));  
i = 1;
while i_end<length(Z)        
    i_end = min(i_start+WINDOW_Z-1,length(Z));    
    FZi = Fvec(Z(i_start:i_end));
    norm_inf_F(i) = max(abs(FZi(:)));
    norm_l2_F(i) = sqrt(max(sum(conj(FZi).*FZi,1)));
    i_start = i_end+1;
    i = i+1;
end
norm_inf_F = max(norm_inf_F);
norm_l2_F = max(norm_l2_F);

max_deg = 50;

% Table with all the calculation output
tab_all = table('Size',[0 11], 'VariableNames', {'level', 'tol_inf', 'rng' , 'svd_up' , 'method', 'tensor',  'time'  , 'deg'   , 'err_g' , 'err_F_l2', 'err_F_inf'}, ...
                               'VariableTypes', {'int8',  'double' , 'int8', 'logical', 'string', 'logical', 'double', 'double', 'double', 'double'  , 'double'   });


%% Sketched AAA - Nr runs
opts.tensor_sketch = false;
opts.max_norm = true;
opts.tol = tol_inf_norm;
opts.deg = max_deg;
opts.error_on_F = false;

tab_make_sketch = table('Size',[0 4], 'VariableNames', {'rng' , 'ell' , 'tenspor', 'time'  }, ...
                                      'VariableTypes', {'int8', 'int8', 'logical', 'double'});
ells = [1 4 8 16 24];
rngs = 1:Nr;
svd_ups = [true];
tensorized = [false true];

warning('off', 'AAA_SKETCH:safety_ratio')

N = size(FZi,1);
for tensor_sketch=tensorized
    for ell = ells
        ell
        for i_rng = rngs
            
            if ell==0
                error('Too big')            
            else
                T=tic();
                rng(i_rng)  
                UV = zeros(N,ell);            
                for i=1:ell
                    % tensorized sketch is computed on vectorized level since 
                    % we do not have a fast matvec for this problem anyway
                    if tensor_sketch 
                        v = kron(randn(sqrt(N),1),randn(sqrt(N),1));
                    else
                        v = randn(N,1);
                    end                    
                    UV(:,i) = v;            
                end 
                GZ = zeros(ell,length(Z));
                % Windowed evaluation of Z            
                i_start = 1;
                i_end = min(i_start+WINDOW_Z-1,length(Z));  
                i = 1;
                while i_end<length(Z)                   
                    i_end = min(i_start+WINDOW_Z-1,length(Z));    
                    FZi = Fvec(Z(i_start:i_end));
                    GZ(:,i_start:i_end) = UV'*FZi;                
                    i_start = i_end+1;
                    i = i+1;
                end            
                time_sketch=toc(T);
                tab_make_sketch = [tab_make_sketch; {i_rng,ell,tensor_sketch,time_sketch}];
            end
    
    
            for svd_up = svd_ups
                skip = false;
                if ell==0 
                    error('ell = 0')                    
                end  
                if ~skip
                    opts.svd_up = svd_up;
                    T=tic();
                    [z,w,ind,stats_faster] = sketchAAA(GZ,Z,max_deg,opts);
                    R = @(zz) eval_bary_vec(zz, z, w, Fvec(z));   
                    time = toc(T) + time_sketch;
                    [norm_inf_err, norm_l2_err] = compute_errors_loop(Fvec,R,Z, WINDOW_Z);
                    tab_all = [tab_all; {meshlevel,tol_inf_norm,i_rng,svd_up,sprintf('ell=%02i',ell),tensor_sketch,time,length(z),stats_faster.err_g(end),norm_l2_err/norm_l2_F,norm_inf_err/norm_inf_F}];            
                end
            end
        end
    end
end

tab_make_sketch_sum = groupsummary(tab_make_sketch,{'ell'},{"mean", "std"} ,{'time'});
tab_summ = groupsummary(tab_all,{'level', 'tol_inf', 'svd_up', 'method' 'tensor'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf'});

% tab_orig
tab_make_sketch_sum
tab_summ

opts = rmfield(opts, {'svd_up'});

savefile = sprintf('table_bem_large_level%02i_tol%.1e.mat', meshlevel, tol_inf_norm);
save(strcat(save_dir,savefile), 'tab_all', 'opts')

savefile = sprintf('R_bem_large_ell%02i_level%02i_tol%.1e.mat', ell, meshlevel, tol_inf_norm);
save(strcat(save_dir,savefile), 'R')


end


function [maxerr, L2err] = compute_errors_loop(Fvec,R,Z, WINDOW_Z)
% F is matrix with F evaluations
% F(:,i) is error of F(Z(i))

nb_eval = floor(length(Z)/WINDOW_Z) + rem(length(Z),WINDOW_Z);
maxerrs = zeros(nb_eval,1);
L2errs = zeros(nb_eval,1);
i_start = 1;
i_end = min(i_start+WINDOW_Z-1,length(Z));  
i = 1;
while i_end<length(Z)       
    i_end = min(i_start+WINDOW_Z-1,length(Z));    
    FZi = Fvec(Z(i_start:i_end));
    RZi = R(Z(i_start:i_end));
    Ei = FZi - RZi;
    maxerrs(i) = max(abs(Ei(:)));
    L2errs(i) = sqrt(max(sum(conj(Ei).*Ei,1))); %norm(Ei,'fro');      
    i_start = i_end+1;
    i = i+1;
end
maxerr = max(maxerrs);
L2err = max(L2errs);
end

