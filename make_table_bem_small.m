% Make a latex snippet to report the experiment as done in the paper.
% Ready for copy paste with some minor formatting afterwards.

load('result_benchmarks_11_2022/table_benchmark_bem_small.mat')

% Choose manually:
TOL = 1e-12;
%TOL = 1e-08;

T = tab_summ;
T(T.tol_inf==TOL,:)


rows = (T.tol_inf==TOL) & (T.svd_up==true) & (T.tensor==false); 
T = T(rows,:);

cols = ["method", "mean_time", "mean_deg", "mean_err_F_inf"];
T = T(:,cols);

table_size = size(T) ; 
rows = table_size(1); 
line = "";
for row = 1:rows-1 
    m_t_d_e = table2array(T(row,:));
    ell = extractAfter(m_t_d_e(1),"ell=");
    line = sprintf(" %s \\n  %s  &  %0.3f & %0.1f  &  %0.1e || ", line, ell, m_t_d_e(2), m_t_d_e(3), m_t_d_e(4));
end 

fprintf(strrep(line,"|", "\\"))



