Convert eps to pdf with epspdf (included in most latex distributions)
and tight bbox:
  epspdf -b nice_example_F.eps
