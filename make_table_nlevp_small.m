% Make a latex snippet to report the experiment as done in the paper.
% Ready for copy paste with some minor formatting afterwards.

load('result_benchmarks_11_2022/table_benchmark_sv_small.mat')

% Choose manually:
TOL = 1e-08;
% TOL = 1e-12;


T = tab_summ;

rows = (T.tol_inf==TOL) & (T.svd_up==true) & ismember(T.method,["ell=00","ell=01","ell=04","orig_f"]);
T = T(rows,:);

cols = ["kp", "method", "mean_time", "mean_deg", "mean_err_F_inf"];
T = T(:,cols);

T
latex = "|begin{tabular}{lcccccc}";

ks = sort(double(unique(T.kp)), 'ascend');
for ik = 1:length(ks)
    k = ks(ik);
%   
        
    line1 = sprintf(" %d  & ", k);
    line2 = sprintf("    & ");

    % This is SV AAA on f (vector of scalar-valued functions)    
    
    t_d_e = table2array( T((T.kp==k) & (T.method=="orig_f"), ["mean_time", "mean_deg", "mean_err_F_inf"]) );    
    line1 = sprintf(" %s     %2g  &  %0.1e", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s    |multicolumn{2}{c}{ %0.3f } ", line2, t_d_e(1));

    % This is SV AAA on F (vectorized matrix-valued function)
    t_d_e = table2array( T((T.kp==k) & (T.method=="ell=00"), ["mean_time", "mean_deg", "mean_err_F_inf"]) );
    line1 = sprintf(" %s  &  %2g  &  %0.1e", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s  &  |multicolumn{2}{c}{ %0.3f } ", line2, t_d_e(1));
    
    % This is sketchedAAA on F with 1 surrogate
    t_d_e = table2array( T((T.kp==k) & (T.method=="ell=01"), ["mean_time", "mean_deg", "mean_err_F_inf"]) );
    line1 = sprintf(" %s  &  %2g  &  %0.1e ", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s  &  |multicolumn{2}{c}{ %0.3f } ", line2, t_d_e(1));

    % This is sketchedAAA on F with 4 surrogates
    t_d_e = table2array( T((T.kp==k) & (T.method=="ell=04"), ["mean_time", "mean_deg", "mean_err_F_inf"]) );
    line1 = sprintf(" %s  &  %2g  &  %0.1e || ", line1, t_d_e(2), t_d_e(3));
    line2 = sprintf(" %s  &  |multicolumn{2}{c}{ %0.3f } || ", line2, t_d_e(1));

    latex = sprintf("%s \\n %s \\n %s", latex, line1, line2);
        
end

line = "|end{tabular}";
latex = sprintf("%s \\n %s \n", latex, line);


fprintf(strrep(latex,"|", "\\"))