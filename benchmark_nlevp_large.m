function benchmark_nlevp_large()
% Benchmark problems from nlevp that are too large for F(Z)
% to fit in memory.

% It is advisable to run this on a computing node. Takes about 1h even on a
% beefy multicore machine.
%
% Results of the computation (including timings) are stored in
% the folder save_dir as MATLAB tables. 

% BV - 11.2022

save_dir = 'result_benchmarks_11_2022/';

kps = [6 16 17 24]; % not done in benchmark_sketchAAA_small.m
tols = [1e-8 1e-12];
Nsamples = 10; % number of random runs per surrogate

tab_summ = {}; % the final table with the summary of the calculation 
tab_make_sketch = {};

for kp = kps
    % F(z) = sum_i funf(z)
    [F, n, pname, Z, ~, ~, funf, coeffs] = get_nlevp_problem(kp);      

    name_problem = sprintf('nlevp_%02i_%s', kp, pname);
    name_problem
    
    % compute the problem kp for each tol 
    for tol=tols            
        [tab_summ_kp, tab_make_sketch_kp] = run_one_problem(F, funf, coeffs, Z, tol, kp, Nsamples, strcat(save_dir, 'all/'));
                                     
        tab_summ = [tab_summ; tab_summ_kp];
        tab_make_sketch = [tab_make_sketch; tab_make_sketch_kp];
    end    
end

% save tables
savefile = sprintf('table_benchmark_sv_large.mat');
save(strcat(save_dir,savefile), 'tab_summ', 'tab_make_sketch');

tab_summ

end

function [tab_summ, tab_make_sketch] = run_one_problem(F, funf, coeffs, Z, tol_inf_norm, kp, Nr, save_dir)
% benchmark one problem for many values of ell

T = tic();
norm_inf_F = -1;
norm_l2_F = -1;
for i=1:length(Z)
    FZi = F(Z(i));
    norm_inf_F = max([norm_inf_F max(abs(FZi(:)))]);
    norm_l2_F = max([norm_l2_F norm(FZi,'fro')]);
end
time_norm_F = toc(T);

max_deg = 50;

% Table with all the calculation output
tab_all = table('Size',[0 13], 'VariableNames', {'kp'  , 'tol_inf', 'rng' , 'svd_up' , 'method', 'tensor',  'split',  'time'  , 'deg'   , 'err_g' , 'err_F_l2', 'err_F_inf', 'time_err'}, ...
                               'VariableTypes', {'int8', 'double' , 'int8', 'logical', 'string', 'logical', 'logical', 'double', 'double', 'double', 'double'  , 'double'   , 'double'});

%% Original SV-AAA (with svd updating) on vector f 
% construct fZ is r x M        
T = tic();
fZ = zeros(length(funf(Z(1))), length(Z));
for i = 1:length(Z)
    fZi = funf(Z(i));
    fZ(:,i) = fZi(:);       
end
time_fZ = toc(T);

T=tic();
[r, pol, res, zer, z, f, w, errvec] = sv_aaa(fZ.', Z, 'mmax', max_deg+1, 'tol', tol_inf_norm, 'scale_each_F', true);
R = contruct_R(z, w, F);
time = toc(T);

T = tic();
[norm_inf_err, norm_l2_err] = compute_errors_loop(F, R, Z);
time_err = toc(T);

% SV-AAA exploits split form
tab_all = [tab_all; {kp,tol_inf_norm,0,true,'orig_f',0,true,time,length(z),errvec(end),norm_l2_err/norm_l2_F,norm_inf_err/norm_inf_F, time_err}];            

% CANNOT STORE FZ - so skip SV-AAA F

%% Sketched AAA - Nr runs
opts.max_norm = true;
opts.tol = tol_inf_norm;
opts.deg = max_deg;

tab_make_sketch = table('Size',[0 4], 'VariableNames', {'rng' , 'ell' , 'tensor',  'time'  }, ...
                                      'VariableTypes', {'int8', 'int8', 'logical', 'double'});
ells = [1 4 8 12];  % ell=0 is too costly, this is essentially SV-AAA F
rngs = 1:Nr;
tensors = [true false];
split_forms = [true false];

warning('off', 'AAA_SKETCH:safety_ratio')

FZ1 = F(Z(1)); psize = size(FZ1,1);
N = size(FZ1(:),1);
for split_form = split_forms
    for ell = ells
        for i_rng = rngs
            rng(i_rng)
            for tensor = tensors
                pass = false;
                
                T=tic();
                if tensor % tensor sketch
                    U = randn(ell,psize); V = randn(psize,ell);              
                    if split_form % exploit split form
                        for j_coeff=1:length(coeffs)
                            for i_ell = 1:ell
                                sketchA(i_ell,j_coeff) = U(i_ell,:)*coeffs{j_coeff}*V(:,i_ell);
                            end
                        end
                        GZ = sketchA * (funf(Z).');
                    else % do not exploit split form
                        GZ = zeros(ell,length(Z));
                        for j = 1:length(Z)
                            Fz = F(Z(j));
                            for i_ell = 1:ell
                                GZ(i_ell,j) = U(i_ell,:)*Fz*V(:,i_ell);
                            end
                        end
                    end
                else % no tensor sketch
                    if split_form % exploit split form not implemented                        
                        % TODO                        
                        sketchA = sparseprobe_cell(coeffs,ell,false,i_rng);        
                        GZ = sketchA * (funf(Z).');
                    else
                        GZ = sparseprobe(F,Z,ell,false,i_rng);              
                    end
                end          
                time_sketch=toc(T);
    
                if ~pass
                    tab_make_sketch = [tab_make_sketch; {i_rng,ell,tensor,time_sketch}];
        
                    T=tic();
                    [z,w,stats_faster] = sketchAAA(GZ,Z,max_deg,opts);
                    R = contruct_R(z, w, F);
                    time = toc(T) + time_sketch;
                    T = tic();
                    [norm_inf_err, norm_l2_err] = compute_errors_loop(F,R,Z);
                    time_err = toc(T);
                    tab_all = [tab_all; {kp,tol_inf_norm,i_rng,true,sprintf('ell=%02i',ell),tensor,split_form,time,length(z),stats_faster.err_g(end),norm_l2_err/norm_l2_F,norm_inf_err/norm_inf_F, time_err}];            
                end
            end
        end
    end
end

tab_make_sketch_sum = groupsummary(tab_make_sketch,{'ell'},{"mean", "std"} ,{'time'});
tab_summ = groupsummary(tab_all,{'kp', 'tol_inf', 'svd_up', 'method','tensor','split'},{"mean", "std"} ,{'time', 'deg', 'err_g', 'err_F_l2', 'err_F_inf', 'time_err'});

tab_make_sketch_sum
tab_summ

savefile = sprintf('table_benchmark_sv_large_%02i_tol%.1e.mat', kp, tol_inf_norm);
save(strcat(save_dir,savefile), 'tab_all', 'opts')

end

function [maxerr, L2err] = compute_errors_loop(F,R,Z)
% F is matrix with F evaluations
% F(:,i) is error of F(Z(i))
for i=1:length(Z)
    FZi = F(Z(i));
    RZi = R(Z(i));
    Ei = FZi(:) - RZi(:);  
    maxerrs(i) = full(max(abs(Ei)));  % Do not use norm(Ei, 'inf') -- it is very slow for sparse matrices
    L2errs(i) = norm(Ei,2);  
end
maxerr = max(maxerrs);
L2err = max(L2errs);
end



