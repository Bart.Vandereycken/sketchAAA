function R = eval_bary_vec(z,zk,wk,Fk)
% Evaluate vector-valued (of size N) barycentric interpolant of degree d
% z can be a vector, Fk is N x d matrix with d degree.
% Loops over the degree, vectorized in z.

% Get the rational approximation. Same evaluation of barycentric formula
% as in sketchAAA.
M = length(z);
[nG,mmax] = size(Fk);
g = Fk.';
w = wk;

GZ = zeros(nG,M);
C = zeros(M,mmax);
for m=1:mmax
    C(:,m) = 1./(z - zk(m));

end

N = C*(w.*g(1:m,:));       % Numerator
D = C*(w.*ones(m,nG));     % Denominator
R = N./D;

R = R.';

for i=1:length(zk)
    [val,ind] = min(abs(z-zk(i)));
    if val < 1e1*eps
        R(:,ind) = Fk(:,i);
    end
end



% The code above is mathematically equivalent to the code below
% which is a more direct calculation. It is also less numerically accurate.

% N = 0*Fk(:,1); % numerator (might be sparse)
% d = 0;       % denominator
% 
% z = z(:);
% Z = ones(size(Fk,1),1)*z.';
% for j = 1:length(zk)
%     N = N + (wk(j)./(Z-zk(j))).*Fk(:,j);
%     d = d + wk(j)./(Z-zk(j));
% end
% R = N./d;


end

