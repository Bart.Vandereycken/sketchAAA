function [F, pname, N, Z] = get_bem_problem(meshlevel, gauss)
% F(z) is the vectored bem matrix of size N*N
%    evaluating F for a vector of size m is a lot cheaper than m times for
%    a scalar
%
% gauss is 3, 7, 13 (higher is more accurate but evalating F is more
%                    expensive)
% meshlevel is 3, 5, 7, ... (size of BEM matrix is about
% Use this to control the level of mesh refinement. Large meshlevel result
% in larger matrices.

% From C. Effenberger and D. Kressner, 
% Chebyshev interpolation for nonlinear eigenvalue problems, BIT, 52 (2012), pp. 933–951.



addpath('bem')

% Generates an NxNx#nodes array containing the evaluations of T on the nodes
% in each slice.

domain = 'fichera';      % or 'unitcube'

% generate boundary mesh for Fichera corner
fprintf('Generating boundary mesh ...              '); tic
mesh = genmesh(meshlevel, domain);
fprintf('Done in %.6f seconds.\n\n', toc);

N = length(mesh);

    function A = make_mat(z)
        A = assembleT( z, mesh, gauss);
        A = reshape(A, [N*N length(z)]);
    end
    F = @make_mat;

pname = 'bem_fichera';
Z = linspace(5,12,200); 



end


