function R = eval_bary(z,zk,wk,Fk)
% Evaluate a matrix-valued barycentric interpolant.

[val,ind] = min(abs(z-zk)); % evaluation at support point
if val < 1e1*eps
    R = Fk{ind};
    return
end

N = 0*Fk{1}; % numerator (might be sparse)
d = 0;       % denominator

for j = 1:length(zk)
    N = N + (wk(j)/(z-zk(j)))*Fk{j};
    d = d + wk(j)/(z-zk(j));
end
R = N/d;

end

