function [vals, ind, V] = sparseprobe_cell(A,ell,verbose,i_rng)
% [vals, ind, V] = sparseprobe_cell(A,ell,verbose)
%   Non-tensorized probing of a cell array A
%   using ell Gaussian random vectors. Probed values are
%   returned in the ell-by-length(A) matrix vals.
%   The vector ind contains the nonzero entries of A
%   and V is the set of probing vectors (stored row-wise).

if nargin < 4
    verbose = 0;
end


rng(i_rng); ind = []; V = [];
vals = zeros(ell,length(A));
for j = 1:length(A)
    Aj = A{j};
    ind1 = find(Aj);
    
    % check if ind1 is different from ind
    % in most cases it's not and we want to avoid calling setdiff
    if length(ind1)~=length(ind) || ... 
        (length(ind1)==length(ind) && any(ind1 ~= ind))
        % NOTE: setdiff is slow and this can probably be improved
        %       as ind and ind1 are sorted
        sd = setdiff(ind1,ind); 
        if ~isempty(sd)
            if verbose
                disp('new nonzeros detected. increasing V')
            end
            ind = [ ind ; sd ];
            V = [ V , randn(ell,length(sd)) ];
            [ind, r] = sort(ind);
            V = V(:,r);
        end
    end

    Aj = Aj(ind);
    vals(:,j) = V*Aj;
end

end

