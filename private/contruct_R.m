function R = contruct_R(z, w, F)
% Construct a function to evaluate a rational approximation R in
% barycentric form.

Fk = cell(length(z),1);
for ii=1:length(z)
    Fk{ii} = F(z(ii));
end
R = @(zz) eval_bary(zz, z, w, Fk);