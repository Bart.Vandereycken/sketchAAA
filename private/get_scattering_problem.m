function [F, N, pname, Z] = get_scattering_problem()

addpath('scattering')

K = 0; C = 0; M = 0; b = 0;

load('K.mat', 'K'), load('C.mat'), load('M.mat'), load('b.mat')
b = full(b);

    function v = F_sc(z)
        % z must be real
        v = zeros(length(b),length(z));
        for ii=1:length(z)
            v(:,ii) = ( K - 1j * z(ii) * C - z(ii)^2 * M ) \ b;
        end
    end
F = @F_sc;
N = length(b);
pname = 'scattering';

% If not enough samples are used, the error is underestimated on the 
% interval. This will also be evident from "small g but large F" error when
% sketching. We take 400 to be sure.
Z = linspace(5,10,400);

end

